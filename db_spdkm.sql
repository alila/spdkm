-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 25, 2018 at 03:03 PM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_spdkm`
--

-- --------------------------------------------------------

--
-- Table structure for table `kota`
--

CREATE TABLE `kota` (
  `no` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kota`
--

INSERT INTO `kota` (`no`, `nama`) VALUES
(1, 'Malang'),
(2, 'Solo'),
(3, 'Yogyakarta'),
(4, 'Jakarta'),
(5, 'Madiun'),
(6, 'Ngawi'),
(7, 'Magelang'),
(8, 'Surabaya'),
(9, 'Purworejo'),
(10, 'Kediri'),
(11, 'Tulungagung');

-- --------------------------------------------------------

--
-- Table structure for table `tb_admin`
--

CREATE TABLE `tb_admin` (
  `nip` varchar(50) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `password` varchar(10) NOT NULL,
  `foto_adm` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_admin`
--

INSERT INTO `tb_admin` (`nip`, `nama`, `password`, `foto_adm`) VALUES
('12345678', 'Admin1', 'admin', 'file-foto1535135574.png');

-- --------------------------------------------------------

--
-- Table structure for table `tb_aktif`
--

CREATE TABLE `tb_aktif` (
  `id_aktif` int(11) NOT NULL,
  `nim` varchar(50) NOT NULL,
  `no_surat` varchar(30) NOT NULL,
  `keperluan` text NOT NULL,
  `tgl_pengajuan` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_aktif`
--

INSERT INTO `tb_aktif` (`id_aktif`, `nim`, `no_surat`, `keperluan`, `tgl_pengajuan`) VALUES
(9, '2014.02475.11.0701', 'B/9/FTTI-UNJANI/VIII/2018', 'lulululu', '2018-08-16'),
(10, '2014.02475.11.0701', 'B/10/FTTI-UNJANI/VIII/2018', 'lolololo', '2018-08-16'),
(11, '2014.02475.11.0701', 'B/11/FTTI-UNJANI/VIII/2018', 'kekeke', '2018-08-17'),
(12, '2014.02475.11.0701', 'B/12/FTTI-UNJANI/VIII/2018', 'LALALA', '2018-08-17'),
(13, '2014.02475.11.0701', 'B/13/FTTI-UNJANI/VIII/2018', '                ', '2018-08-17'),
(14, '2014.02475.11.0701', 'B/14/FTTI-UNJANI/VIII/2018', '                ', '2018-08-17'),
(16, '12345678', 'B/15/FTTI-UNJANI/VIII/2018', '                ', '2018-08-19'),
(17, '12345678', 'B/16/FTTI-UNJANI/VIII/2018', '                ', '2018-08-19'),
(18, '2015.112.11.11.111', 'B/9/FTTI-UNJANI/VIII/2018', 'Pengajuan', '2018-08-25');

-- --------------------------------------------------------

--
-- Table structure for table `tb_kkn`
--

CREATE TABLE `tb_kkn` (
  `id_kkn` int(11) NOT NULL,
  `nim` varchar(50) NOT NULL,
  `no_surat` varchar(30) NOT NULL,
  `tempat_kkn` text NOT NULL,
  `tgl_mulai` date NOT NULL,
  `tgl_selesai` date NOT NULL,
  `tertuju` varchar(20) NOT NULL,
  `kode_surat` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_kkn`
--

INSERT INTO `tb_kkn` (`id_kkn`, `nim`, `no_surat`, `tempat_kkn`, `tgl_mulai`, `tgl_selesai`, `tertuju`, `kode_surat`) VALUES
(8, '2014.02475.11.0701', '', '', '0000-00-00', '0000-00-00', '', '0'),
(19, '', '3/C.21TI/02/VIII/2018', '', '0000-00-00', '0000-00-00', '', '0'),
(20, '', '4/C.21TI/02/VIII/2018', '', '0000-00-00', '0000-00-00', '', '0'),
(21, '', '5/C.21TI/02/VIII/2018', 'Desa Kendal', '2018-07-30', '2018-08-22', 'Kades', '0'),
(23, '', '7/C.21TI/02/VIII/2018', '', '0000-00-00', '0000-00-00', '', '0'),
(25, '', '9/C.21TI/02/VIII/2018', '', '0000-00-00', '0000-00-00', '', '0'),
(27, '2014.02475.11.0701', '', '', '0000-00-00', '0000-00-00', '', '0'),
(28, '', '12/C.21TI/02/VIII/2018', '', '0000-00-00', '0000-00-00', '', '0'),
(30, '', '14/C.21TI/02/VIII/2018', '', '0000-00-00', '0000-00-00', '', '0'),
(32, '2014.02416.11.0642', '16/A.00/05/VIII/2018', 'Desa Banyuraden', '2018-08-17', '2018-08-31', 'Kades', 's_penelitian_16'),
(34, '2014.02416.11.0642', '13/A.00/05/VIII/2018', 'lilili', '2018-08-01', '2018-08-24', 'Pimpinan', 's_penelitian_13'),
(35, '2014.02475.11.0701', '13/A.00/05/VIII/2018', 'lilili', '2018-08-01', '2018-08-24', 'Pimpinan', 's_penelitian_13'),
(36, '2014.02416.11.0642', '13/A.00/05/VIII/2018', 'lilili', '2018-08-01', '2018-08-24', 'Pimpinan', 's_penelitian_13'),
(37, '2014.02416.11.0642', '14/A.00/05/VIII/2018', 'bababa', '2018-08-03', '2018-08-23', 'Pimpinan', 's_penelitian_14'),
(38, '2014.02416.11.0642', '15/A.00/05/VIII/2018', 'lelele', '2018-08-01', '2018-08-31', 'Pimpinan', 's_penelitian_15'),
(39, '2014.02416.11.0642', '16/A.00/05/VIII/2018', '', '0000-00-00', '0000-00-00', '', 's_penelitian_16'),
(40, '2014.02416.11.0642', '17/A.00/05/VIII/2018', 'Desa Kendal', '2018-08-01', '2018-08-31', 'Kades', 's_penelitian_17'),
(41, '2014.02416.11.0642', '18/A.00/05/VIII/2018', '', '0000-00-00', '0000-00-00', '', 's_penelitian_18'),
(42, '2014.02416.11.0642', '19/A.00/05/VIII/2018', 'Desa Wonosari', '2018-08-24', '2018-09-07', 'Kades', 's_penelitian_19');

-- --------------------------------------------------------

--
-- Table structure for table `tb_magang`
--

CREATE TABLE `tb_magang` (
  `id_magang` varchar(50) NOT NULL,
  `no_surat` varchar(30) NOT NULL,
  `nama_instansi` varchar(50) NOT NULL,
  `tgl_mulai` date NOT NULL,
  `tgl_selesai` date NOT NULL,
  `kode_form` varchar(20) NOT NULL,
  `alamat_instansi` text NOT NULL,
  `tertuju` varchar(20) NOT NULL,
  `no_telp` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_magang`
--

INSERT INTO `tb_magang` (`id_magang`, `no_surat`, `nama_instansi`, `tgl_mulai`, `tgl_selesai`, `kode_form`, `alamat_instansi`, `tertuju`, `no_telp`) VALUES
('ID_MG_10', '10/C/FTTI-UNJANI/VIII/2018', 'BCA', '2018-07-30', '2018-08-31', 'FRM_MG_10', 'Jl. Godean', 'Pimpinan', 0),
('ID_MG_11', '11/C.21TI/02/VIII/2018', '', '0000-00-00', '0000-00-00', 'FRM_MG_11', '', '', 0),
('ID_MG_12', '12/C.21TI/02/VIII/2018', '', '0000-00-00', '0000-00-00', 'FRM_MG_12', '', '', 0),
('ID_MG_13', '13/C.21TI/02/VIII/2018', '', '0000-00-00', '0000-00-00', 'FRM_MG_13', '', '', 0),
('ID_MG_14', '14/C.21TI/02/VIII/2018', '', '0000-00-00', '0000-00-00', 'FRM_MG_14', '', '', 0),
('ID_MG_15', '15/C.21TI/02/VIII/2018', '', '0000-00-00', '0000-00-00', 'FRM_MG_15', '', '', 0),
('ID_MG_16', '16/C.21TI/02/VIII/2018', '', '0000-00-00', '0000-00-00', 'FRM_MG_16', '', '', 0),
('ID_MG_17', '17/C.21TI/02/VIII/2018', '', '0000-00-00', '0000-00-00', 'FRM_MG_17', '', '', 0),
('ID_MG_18', '18/C.21TI/02/VIII/2018', '', '0000-00-00', '0000-00-00', 'FRM_MG_18', '', '', 0),
('ID_MG_19', '19/C.21TI/02/VIII/2018', '', '0000-00-00', '0000-00-00', 'FRM_MG_19', '', '', 0),
('ID_MG_2', '2/C/FTTI-UNJANI/VIII/2018', 'BCA', '2018-07-30', '2018-08-31', 'FRM_MG_2', 'Jl. Godean', 'Pimpinan', 0),
('ID_MG_20', '20/C.21TI/02/VIII/2018', '', '0000-00-00', '0000-00-00', 'FRM_MG_20', '', '', 0),
('ID_MG_21', '21/C.21TI/02/VIII/2018', 'BCA', '2018-07-30', '2018-08-17', 'FRM_MG_21', 'Jl. Godean', 'Pimpinan', 0),
('ID_MG_22', '22/C.21TI/02/VIII/2018', 'BRI', '2018-07-30', '2018-08-17', 'FRM_MG_22', 'Jl. Wates', 'Pimpinan', 0),
('ID_MG_23', '23/C.21TI/02/VIII/2018', '', '0000-00-00', '0000-00-00', 'FRM_MG_23', '', '', 0),
('ID_MG_24', '24/C.21TI/02/VIII/2018', '', '0000-00-00', '0000-00-00', 'FRM_MG_24', '', '', 0),
('ID_MG_25', '25/C.21TI/02/VIII/2018', '', '0000-00-00', '0000-00-00', 'FRM_MG_25', '', '', 0),
('ID_MG_26', '26/C.21TI/02/VIII/2018', 'BCA', '2018-07-30', '2018-08-30', 'FRM_MG_26', 'Jl. Godean', 'Pimpinan', 0),
('ID_MG_27', '27/C.21TI/02/VIII/2018', '', '0000-00-00', '0000-00-00', 'FRM_MG_27', '', '', 0),
('ID_MG_28', '28/C.21TI/02/VIII/2018', 'BCA', '2018-07-30', '2018-08-30', 'FRM_MG_28', 'Jl. Godean', 'Pimpinan', 0),
('ID_MG_29', '29/C.21TI/02/VIII/2018', 'BCA', '2018-07-31', '2018-08-21', 'FRM_MG_29', 'Jl. Godean', 'Pimpinan', 0),
('ID_MG_3', '3/C/FTTI-UNJANI/VIII/2018', '', '0000-00-00', '0000-00-00', 'FRM_MG_3', '', '', 0),
('ID_MG_30', '30/C.21TI/02/VIII/2018', 'BRI', '2018-08-01', '2018-08-28', 'FRM_MG_30', 'Jl. Wates', 'Pimpinan', 0),
('ID_MG_31', '31/C.21TI/02/VIII/2018', 'UNJANI', '2018-07-30', '2018-08-31', 'FRM_MG_31', 'Jl. Ring Road Barat Gamping', 'Rektor', 0),
('ID_MG_32', '32/C.21TI/02/VIII/2018', '', '0000-00-00', '0000-00-00', 'FRM_MG_32', '', '', 0),
('ID_MG_33', '33/C.21TI/02/VIII/2018', 'BRI', '2018-08-01', '2018-08-29', 'FRM_MG_33', 'Jl. Wates', 'Pimpinan', 0),
('ID_MG_34', '34/C.21TI/02/VIII/2018', '', '0000-00-00', '0000-00-00', 'FRM_MG_34', '', '', 0),
('ID_MG_35', '35/C.21TI/02/VIII/2018', '', '0000-00-00', '0000-00-00', 'FRM_MG_35', '', '', 0),
('ID_MG_36', '36/C.21TI/02/VIII/2018', '', '0000-00-00', '0000-00-00', 'FRM_MG_36', '', '', 0),
('ID_MG_37', '37/C.21TI/02/VIII/2018', '', '0000-00-00', '0000-00-00', 'FRM_MG_37', '', '', 0),
('ID_MG_38', '38/C.21TI/02/VIII/2018', '', '0000-00-00', '0000-00-00', 'FRM_MG_38', '', '', 0),
('ID_MG_39', '39/C.21TI/02/VIII/2018', '', '0000-00-00', '0000-00-00', 'FRM_MG_39', '', '', 0),
('ID_MG_4', '4/C/FTTI-UNJANI/VIII/2018', '', '0000-00-00', '0000-00-00', 'FRM_MG_4', '', '', 0),
('ID_MG_40', '40/C.21TI/02/VIII/2018', 'BCA', '2018-07-31', '2018-08-10', 'FRM_MG_40', 'Jl. Godean', 'Pimpinan', 0),
('ID_MG_41', '41/C.21TI/02/VIII/2018', 'Universitas Jenderal Achmad Yani ', '2018-07-31', '2018-09-07', 'FRM_MG_41', 'Jl. Ring Road Barat Gamping', 'Rektor', 0),
('ID_MG_42', '42/C.21TI/02/VIII/2018', 'BNN', '2018-08-07', '2018-08-24', 'FRM_MG_42', 'Kota Gede', 'Direktur', 0),
('ID_MG_43', '43/C.21TI/02/VIII/2018', 'lalala', '2018-08-01', '2018-08-10', 'FRM_MG_43', 'lalala', 'Pimpinan', 0),
('ID_MG_44', '44/C.21TI/02/VIII/2018', 'UNJANI', '2018-07-31', '2018-08-31', 'FRM_MG_44', 'Jl. Ring Road Barat Gamping', 'Rektor', 0),
('ID_MG_5', '5/C/FTTI-UNJANI/VIII/2018', '', '0000-00-00', '0000-00-00', 'FRM_MG_5', '', '', 0),
('ID_MG_6', '6/C/FTTI-UNJANI/VIII/2018', 'BCA', '2018-08-01', '2018-08-31', 'FRM_MG_6', 'Jl. Godean', 'Pimpinan', 0),
('ID_MG_7', '7/C/FTTI-UNJANI/VIII/2018', '', '0000-00-00', '0000-00-00', 'FRM_MG_7', '', '', 0),
('ID_MG_8', '8/C/FTTI-UNJANI/VIII/2018', '', '0000-00-00', '0000-00-00', 'FRM_MG_8', '', '', 0),
('ID_MG_9', '9/C/FTTI-UNJANI/VIII/2018', '', '0000-00-00', '0000-00-00', 'FRM_MG_9', '', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tb_mahasiswa`
--

CREATE TABLE `tb_mahasiswa` (
  `nim` varchar(50) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `prodi` varchar(30) NOT NULL,
  `tempat_lahir` varchar(30) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `nama_ortu` varchar(50) NOT NULL,
  `pekerjaan_ortu` varchar(50) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `password` varchar(10) NOT NULL,
  `foto_mhs` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_mahasiswa`
--

INSERT INTO `tb_mahasiswa` (`nim`, `nama`, `prodi`, `tempat_lahir`, `tgl_lahir`, `nama_ortu`, `pekerjaan_ortu`, `alamat`, `password`, `foto_mhs`) VALUES
('2014.02416.11.0642', 'Dhea Febriastuti Candra Devi ', 'S-1 Teknik Informatika', 'Madiun', '1996-02-28', 'Akhmat Bisri', 'TNI AD', 'Ds. Karangsari, Rt. 001/001, Kec. Rowosari, Kab. Kendal ', '11.0642', 'file-foto1535135591.png'),
('2015.112.11.11.111', 'Ahmad Muttaqin', 'TI', 'Yogyakarta', '0000-00-00', 'Dery', 'Penyanyi', 'Jl Raya Palembang RT 01/01 BLOK A No 01 Palembang, Palembang, Sumatra Selatan', 'dery', 'file-foto1535187017.png'),
('2015.112.11.11.112', 'Ahmad Muttaqin', 'TI', 'Yogyakarta', '0000-00-00', 'Dery', 'Penyanyi', 'Jl Raya Palembang RT 01/01 BLOK A No 01 Palembang, Palembang, Sumatra Selatan', 'dery', 'file-foto1535187065.png');

-- --------------------------------------------------------

--
-- Table structure for table `tb_mhs_magang`
--

CREATE TABLE `tb_mhs_magang` (
  `id` int(11) NOT NULL,
  `nim` varchar(50) NOT NULL,
  `total_sks` int(11) NOT NULL,
  `ipk` int(11) NOT NULL,
  `nilai_d` int(11) NOT NULL,
  `nilai_e` int(11) NOT NULL,
  `dosbing` varchar(100) NOT NULL,
  `id_magang` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_mhs_magang`
--

INSERT INTO `tb_mhs_magang` (`id`, `nim`, `total_sks`, `ipk`, `nilai_d`, `nilai_e`, `dosbing`, `id_magang`) VALUES
(3, '2014.02416.11.0642', 0, 0, 0, 0, '', 'ID_MG_30'),
(4, '2014.02475.11.0701', 0, 0, 0, 0, '', 'ID_MG_31'),
(5, '2014.02475.11.0701', 0, 0, 0, 0, '', 'ID_MG_31'),
(6, '2014.02475.11.0701', 0, 0, 0, 0, '', 'ID_MG_32'),
(7, '2014.02475.11.0701', 0, 0, 0, 0, '', 'ID_MG_32'),
(8, '2014.02475.11.0701', 0, 0, 0, 0, '', 'ID_MG_33'),
(9, '2014.02416.11.0642', 0, 0, 0, 0, '', 'ID_MG_34'),
(10, '2014.02416.11.0642', 0, 0, 0, 0, '', 'ID_MG_36'),
(11, '2014.02416.11.0642', 0, 0, 0, 0, '', 'ID_MG_38'),
(12, '2014.02416.11.0642', 0, 0, 0, 0, '', 'ID_MG_40'),
(13, '2014.02416.11.0642', 0, 0, 0, 0, '', 'ID_MG_41'),
(14, '2014.02416.11.0642', 0, 0, 0, 0, '', 'ID_MG_42'),
(15, '2014.02416.11.0642', 0, 0, 0, 0, '', 'ID_MG_43'),
(16, '2014.02416.11.0642', 0, 0, 0, 0, '', 'ID_MG_44');

-- --------------------------------------------------------

--
-- Table structure for table `tb_pegawai`
--

CREATE TABLE `tb_pegawai` (
  `npp` varchar(30) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `jabatan` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_pegawai`
--

INSERT INTO `tb_pegawai` (`npp`, `nama`, `jabatan`) VALUES
('1', 'Indawati, S.E., M.Ak.', 'Ka.Bag. Administrasi AkademiK dan Kemahasiswaan'),
('19731008 200501 1 001', 'Agung Priyanto, S.T., M.Eng.', 'Dosen '),
('19740419 200501 2 002', 'Titik Rahmawati, S.T., M.Cs.', 'Dosen'),
('19770711 200501 1 001', 'Choerun Asnawi, S.Kom.', 'Dosen'),
('1996.13.0001', 'Edhy Tri Cahyono, S.Si., M.M.', 'Dosen'),
('1997.13.0002', 'Ari Cahyono, S.Si., M.T.', 'Dosen'),
('2001.13.0003', 'Damar Widodo, S.Si.', 'Dosen'),
('2001.13.0004', 'Arif Himawan, S.Kom., M.M., M.Eng.', 'Dosen'),
('2003.13.0005', 'Landung Sudarmana, S.T., M.Kom.', 'Dosen'),
('2003.13.0006', 'Dayat Subekti, S.Si.', 'Dosen'),
('2003.13.0007', 'Adkhan Sholeh, S.Si., M.Cs.', 'Dosen'),
('2008.13.0020', 'Ahmad Hanafi, S.T., M.Eng.', 'Dosen'),
('2008.13.0021', 'Chanief Budi Setiawan, S.T., M.Eng.', 'Dosen'),
('2011.13.0049', 'Muhammad Rifqi Ma\'arif, S.T., M.Eng.', 'Dosen'),
('2016.13.0084', 'Sari Wijayanti, S.Kom., M.Kom', 'Dosen'),
('2016.13.0085', 'Burhan Alfironi Muktamar, S.Kom., M.Eng.', 'Ka. Prodi D3-Manajemen Informatika'),
('2016.13.0086', 'Arief Ikhwan Wicaksono, M.Sc.', 'Dosen'),
('2016.13.0087', 'Andika Bayu Saputra, S.Kom., M.Kom.', 'Ka. Prodi S1-Teknik Informatika'),
('2017.13.0099', 'Dr. Djoko Susilo, S.T., M.T.', ''),
('2018.13.0105', 'Alfirna Rizqi Lahitani, S.Kom., M.Eng.', 'Dosen'),
('2018.13.0107', 'Aris Wahyu Murdiyanto, S.Kom., M.Cs.', 'Dosen'),
('2018.13.0108', 'Muhammad Habibi, S.Kom., M.Cs.', 'Dosen'),
('2018.13.0109', 'Puji Winar Cahyo, S.Kom., M.Cs.', 'Dosen'),
('2018.13.0110', 'Adri Priadana, S.Kom., M.Cs.', 'Dosen');

-- --------------------------------------------------------

--
-- Table structure for table `tb_penelitian`
--

CREATE TABLE `tb_penelitian` (
  `id_penelitian` int(11) NOT NULL,
  `nim` varchar(50) NOT NULL,
  `no_surat` varchar(30) NOT NULL,
  `tempat_penelitian` text NOT NULL,
  `tgl_mulai` date NOT NULL,
  `tgl_selesai` date NOT NULL,
  `tertuju` varchar(20) NOT NULL,
  `kode_surat` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_penelitian`
--

INSERT INTO `tb_penelitian` (`id_penelitian`, `nim`, `no_surat`, `tempat_penelitian`, `tgl_mulai`, `tgl_selesai`, `tertuju`, `kode_surat`) VALUES
(1, '', '1/C.21TI/02/VIII/2018', 'lilili', '2018-08-04', '2018-08-25', 'Pimpinan', ''),
(2, '', '', 'lilili', '2018-08-04', '2018-08-25', 'Pimpinan', ''),
(3, '', '3/C.21TI/02/VIII/2018', 'lilili', '2018-08-04', '2018-08-25', 'Pimpinan', ''),
(4, '', '', 'lilili', '2018-08-04', '2018-08-25', 'Pimpinan', ''),
(5, '', '', 'lilili', '2018-08-04', '2018-08-25', 'Pimpinan', ''),
(6, '', '6/C.21TI/02/VIII/2018', 'lilili', '2018-08-04', '2018-08-25', 'Pimpinan', ''),
(7, '', '', 'lilili', '2018-08-04', '2018-08-25', 'Pimpinan', ''),
(8, '', '8/C.21TI/02/VIII/2018', 'lilili', '2018-08-04', '2018-08-25', 'Pimpinan', ''),
(9, '', '', 'lilili', '2018-08-04', '2018-08-25', 'Pimpinan', ''),
(10, '', '10/A.00/05/VIII/2018', 'lilili', '2018-08-04', '2018-08-25', 'Pimpinan', ''),
(11, '', '', 'lilili', '2018-08-04', '2018-08-25', 'Pimpinan', ''),
(13, '2014.02416.11.0642', '13/A.00/05/VIII/2018', 'lalala', '2018-08-07', '2018-08-23', 'ketua', 's_penelitian_13'),
(16, '2014.02416.11.0642', '14/A.00/05/VIII/2018', 'BCA', '2018-08-02', '2018-08-09', 'Pimpinan', 's_penelitian_14'),
(17, '2014.02416.11.0642', '14/A.00/05/VIII/2018', 'BCA', '2018-08-02', '2018-08-09', 'Pimpinan', 's_penelitian_14'),
(20, '2014.02416.11.0642', '15/A.00/05/VIII/2018', '', '0000-00-00', '0000-00-00', '', 's_penelitian_15'),
(21, '2014.02416.11.0642', '16/A.00/05/VIII/2018', '', '0000-00-00', '0000-00-00', '', 's_penelitian_16'),
(22, '2014.02416.11.0642', '17/A.00/05/VIII/2018', '', '0000-00-00', '0000-00-00', '', 's_penelitian_17'),
(23, '2014.02416.11.0642', '18/A.00/05/VIII/2018', 'lilili', '2018-08-01', '2018-08-17', 'Pimpinan', 's_penelitian_18'),
(24, '2014.02416.11.0642', '19/A.00/05/VIII/2018', 'kekekeeke', '2018-08-09', '2018-08-11', 'ketua', 's_penelitian_19'),
(25, '2014.02416.11.0642', '20/A.00/05/VIII/2018', '', '0000-00-00', '0000-00-00', '', 's_penelitian_20');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `kota`
--
ALTER TABLE `kota`
  ADD PRIMARY KEY (`no`);

--
-- Indexes for table `tb_admin`
--
ALTER TABLE `tb_admin`
  ADD PRIMARY KEY (`nip`);

--
-- Indexes for table `tb_aktif`
--
ALTER TABLE `tb_aktif`
  ADD PRIMARY KEY (`id_aktif`),
  ADD KEY `nim` (`nim`);

--
-- Indexes for table `tb_kkn`
--
ALTER TABLE `tb_kkn`
  ADD PRIMARY KEY (`id_kkn`),
  ADD KEY `nim` (`nim`);

--
-- Indexes for table `tb_magang`
--
ALTER TABLE `tb_magang`
  ADD PRIMARY KEY (`id_magang`);

--
-- Indexes for table `tb_mahasiswa`
--
ALTER TABLE `tb_mahasiswa`
  ADD PRIMARY KEY (`nim`);

--
-- Indexes for table `tb_mhs_magang`
--
ALTER TABLE `tb_mhs_magang`
  ADD PRIMARY KEY (`id`),
  ADD KEY `nim` (`nim`),
  ADD KEY `id_magang` (`id_magang`);

--
-- Indexes for table `tb_pegawai`
--
ALTER TABLE `tb_pegawai`
  ADD PRIMARY KEY (`npp`);

--
-- Indexes for table `tb_penelitian`
--
ALTER TABLE `tb_penelitian`
  ADD PRIMARY KEY (`id_penelitian`),
  ADD KEY `nim` (`nim`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `kota`
--
ALTER TABLE `kota`
  MODIFY `no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `tb_aktif`
--
ALTER TABLE `tb_aktif`
  MODIFY `id_aktif` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `tb_kkn`
--
ALTER TABLE `tb_kkn`
  MODIFY `id_kkn` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;
--
-- AUTO_INCREMENT for table `tb_mhs_magang`
--
ALTER TABLE `tb_mhs_magang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `tb_penelitian`
--
ALTER TABLE `tb_penelitian`
  MODIFY `id_penelitian` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
