<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class c_magang extends CI_Controller {

    function __construct() {
        parent::__construct();
        if (!$this->session->userdata('id')) {
            redirect('c_login');
        }
    }

    function ajukan_biodata() {					// langkah 1

        $id_magang 		= $this->m_magang->id_magang(); //manggil model untk kode otomatis 
        $kode_form   	= $this->m_magang->kode_form();
		$urut 			= $this->m_magang->urut();
		$bulan 			= $this->m_magang->romawi(date('m'));
		$tahun			= date('Y');
		$no_surat 		= "$urut/C.21TI/02/$bulan/$tahun";
		$data 			= array(
            'id_magang'     	=> $id_magang,
            'no_surat'      	=> $no_surat, //manggil kode
            'nama_instansi' 	=> $this->input->post('nama_instansi'),
            'tgl_mulai'     	=> $this->input->post('tgl_mulai'),
            'tgl_selesai'		=> $this->input->post('tgl_selesai'),
            'kode_form'       	=> $kode_form, //FRM-MG-01
            'alamat_instansi' 	=> $this->input->post('alamat_instansi'),
            'tertuju' 			=> $this->input->post('tertuju')
        );
        $this->m_magang->simpan_magang_db($data);
        redirect('c_magang/tb_mhs_magang/' . $id_magang);
		
    }

    function form_biodata() {
        $this->load->view('magang/form_biodata_magang');
    }
	function tb_mhs_magang($id_magang) { 		// langkah 2
        $data['mhs']=$this->m_magang->mhs($id_magang);
        $data['id_magang'] 	= $id_magang;
        $this->load->view('magang/tb_mhs_magang', $data);
    }
	function simpan_mhs_magang() { 		// langkah 3
        
        $Mhs 	= $this->input->post('Mhs[]');
		$id_magang 	= $this->input->post('id_magang');
        foreach ($Mhs as $m) { 
			$data = array(
				'nim' 		=> $m,
				'id_magang' => $id_magang
			);
            $this->M_dok->simpan_magang_db($data, $m);
        }redirect('c_magang/cetak_biodata_magang/' . $id_magang);
    }

    function cetak_biodata_magang($id_magang) { 	// langkah 4
        $data['magang1']= $this->m_magang->biodata_magang($id_magang);
		$data['magang']= $this->m_magang->instansi($id_magang);
		$data['mhs'] = $this->m_magang->mhs_biodata($this->session->userdata('id'));
        $this->load->view('magang/cetak_biodata_magang', $data); 
    }

    function tb_biodata() { 	// langkah 5
	
		$data['magang']= $this->m_magang->ijin_magang($this->session->userdata('id'));
        $this->load->view('magang/tb_form_biodata', $data); 
    }

    function form_ijin_magang($id) { 		// langkah 6
        $data['datas'] = $this->db->query("SELECT * FROM tb_mhs_magang where id='$id'")->row();
        $this->load->view('magang/form_ijin_magang', $data);
    }

    function simpan_form_ijin_magang($id) { 		// langkah 7
        // proses edit tb_mhs_magang ipk, sks, dosbing, nilai_e, nilai_d berdasarkan nim=session(bikin model jgn lupa)
		$nim=$this->session->userdata('id');
		$data['mhs'] = $this->Mhs_m->list_mhs($nim);
		$data = array(
			'ipk'=> $this->input->post('ipk'),
			'total_sks'=> $this->input->post('total_sks'),
			'nilai_d'=> $this->input->post('nilai_d'),
			'nilai_e'=> $this->input->post('nilai_e'),
			'dosbing'=> $this->input->post('dosbing')
            );
        $this->m_magang->simpan_form_ijin_magang($id,$data);
        redirect ('c_magang/cetak_form_ijin_magang/'. $this->session->userdata('id'));
        //$this->cetak_form_ijin_magang($this->session->userdata('session')); -->digunakan apabila redirect tidak bisa
    }

    function cetak_form_ijin_magang($nim) { 	// langkah 8
        // manggil model yg isinya query join tb_mahasiswa dan tb_mhs_magang berdasarkan nim=$nim
		$data['magang']= $this->m_magang->ijin_magang($nim);
        $this->load->view('magang/cetak_form_ijin_magang', $data); //jangan lupa $data disini
    }

}

?>