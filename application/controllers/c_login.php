<?php
class c_login extends CI_Controller{

	function index(){
		$this->load->view('login');
		}
	
	function cek(){
		$nim = $this->input->post('nim');
		$pass = $this->input->post('pass');
		
		$cek_login = $this->Login_m->cek_db($nim, $pass);
		if($cek_login->num_rows()==1){
			//untuk menampilkan data dari db
			
			foreach ($cek_login->result() as $row){
				//session
				
				$arr['id']=$row->nim;
				$arr['nama']=$row->nama;
				$this->session->set_userdata($arr);
			}
			redirect('Mhs/home');
		} else {
			// untuk kondisi error
			$this->session->set_flashdata('pesan_error', "NIM dan Password SALAH!");
			redirect('c_login');// nama controller
		}
	}
}
?>