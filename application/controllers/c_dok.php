<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class c_dok extends CI_Controller{
	function __construct(){
		parent::__construct();
			if(!$this->session->userdata('id')){
			redirect('c_login');
		}
	}
	
	//SURAT AKTIF KULIAH
	function hapus_aktif($id_aktif){
		$this->m_aktif->hapus_aktif($id_aktif);
		redirect('c_dok/index');
	}
	function data_aktif_mhs(){
		$nim=$this->session->userdata('id');	
		$data['c_dokumen']= $this->m_aktif->aktif($nim);
		$this->load->view('tabel/tab_aktif', $data);
	}
	function cetak_aktif($id_aktif){
		$data['c_dokumen']= $this->m_aktif->aktif($id_aktif);
		$this->load->view('cetak/cetak_aktif', $data);
	}
	function index(){
		$nim=$this->session->userdata('id');
		$data['c_dokumen'] = $this->m_aktif->ajukan_aktif($nim);
		$this->load->view('tabel/tab_aktif', $data);
	}
	function s_aktif(){
		$this->load->view('dokumen/s_aktif');
	}
	function ajukan_aktif(){
		$urut = $this->m_aktif->urut();		//03-08-2018
		$bulan = $this->m_aktif->romawi(date('m'));
		$tahun=date('Y');
		$no_surat = "B/$urut/FTTI-UNJANI/$bulan/$tahun";
		$data = array(
	
			'nim'=> $this->session->userdata('id'),
			'no_surat'=> $no_surat,
			'keperluan'=> $this->input->post('keperluan'),
			'tgl_pengajuan'=> date('Y-m-d'),
			);
		$this->m_aktif->proses_input_db($data);
		redirect('c_dok');
	}
	
	//SURAT MAGANG Start
	function data_magang_mhs(){
		$nim=$this->session->userdata('id');	
		$data['c_dokumen']= $this->m_magang->magang($nim);
		$this->load->view('tabel/tab_magang', $data);
	}
	function cetak_magang($id){
		$data['c_dokumen']= $this->m_magang->instansi($id);
		$data['c_dokumen1']= $this->m_magang->mhs($id);
		$this->load->view('cetak/cetak_magang', $data);
	}
	//-----
	function s_magang(){
		$this->load->view('dokumen/s_magang');
	}
	function ajukan_magang(){
		$urut = $this->m_magang->urut();
		$bulan = $this->m_magang->romawi(date('m'));
		$tahun=date('Y');
		$no_surat = "$urut/C/FTTI-UNJANI/$bulan/$tahun";
		$id_magang = $this->M_dok->id_magang();
		$data = array(
			'id_magang'=> $id_magang,
			'nim'=> $this->input->post('nim'),
			'no_surat'=> $no_surat,
			'nama_instansi'=> $this->input->post('nama_instansi'),
			'tgl_mulai'=> $this->input->post('tgl_mulai'),
			'tgl_selesai'=> $this->input->post('tgl_selesai'),
			);
		$this->M_dok->simpan_magang_db($data);
		redirect('c_dok/tampil_mhs/'.$id_magang);	
		} 
	
	function tampil_mhs($nim){
		$data['Mhs'] = $this->Mhs_m->tampil_mhs();
		$data['id'] = $nim;
		$this->load->view('mahasiswa/tb_mhs', $data);
	}
	function simpan_mhs_magang(){
		$Mhs = $this->input->post('Mhs[]');
		$id = $this->input->post('id_magang');
		foreach($Mhs as $m){
			$data = array(
			'id_magang' => $id
			);
			$this->M_dok->update_mhs_magang($data, $m);
		} redirect('c_dok/data_magang_mhs/'.$id); //functin cetak magang
	}
	//Surat Magang End
	
	//SURAT PENELITIAN
	function data_penelitian_mhs(){
	$nim=$this->session->userdata('id');	
		$data['c_dokumen']= $this->m_penelitian->penelitian($nim);
		$this->load->view('tabel/tab_penelitian', $data);
	}
	function cetak_penelitian($id){
		$data['c_dokumen']= $this->m_penelitian->instansi_penelitian($id);
		$data['c_dokumen2']= $this->m_penelitian->mhs_penelitian($id);
		$this->load->view('cetak/cetak_penelitian', $data);
	}
	function s_penelitian(){
		$this->load->view('dokumen/s_penelitian');
	}
	function ajukan_penelitian(){
		$urut = $this->m_penelitian->urut();
		$bulan = $this->m_penelitian->romawi(date('m'));
		$tahun=date('Y');
		$no_surat = "$urut/A/FTTI-UNJANI/$bulan/$tahun";
		$id_penelitian = $this->M_dok->id_penelitian();
		$data = array(
			'id_penelitian'=> $id_penelitian,
			'no_surat'=> $no_surat,
			'tujuan'=> $this->input->post('tujuan'),
			'tgl_mulai'=> $this->input->post('mulai'),
			'tgl_selesai'=> $this->input->post('selesai')
			);
		$this->M_dok->simpan_penelitian_db($data);
		redirect('c_dok/tampil_mhs1/'.$id_penelitian);	
		} 
	function tampil_mhs1($id){
		$data['Mhs'] = $this->Mhs_m->tampil_mhs1();
		$data['id'] = $id;
		$this->load->view('mahasiswa/tb_penelitian', $data);
	}
	function simpan_mhs_penelitian(){
		$Mhs = $this->input->post('Mhs[]');
		$id = $this->input->post('id_penelitian');
		foreach($Mhs as $p){
			$data = array(
			'id_penelitian' => $id
			);
			$this->M_dok->update_mhs_penelitian($data, $p);
		} redirect('c_dok/cetak_penelitian/'.$id);
	}
	
	
		
		
		
	
	
	
}
?>