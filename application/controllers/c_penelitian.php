<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class c_penelitian extends CI_Controller {

    function __construct() {
        parent::__construct();
        if (!$this->session->userdata('id')) {
            redirect('c_login');
        }
    }
	function tb_mhs_penelitian() { //1
        //menampilkan data mahasiwa semua mahasiswa
        $data['mhs'] = $this->m_penelitian->mhs();
        $this->load->view('penelitian/tb_mhs_penelitian', $data); //$data
    }
	 function simpan_mhs_penelitian() { //2
//        insert data ke tb_penelitian
        $mhs = $this->input->post('Mhs[]');
		$urut 		= $this->m_penelitian->urut();
		$bulan 		= $this->m_penelitian->romawi(date('m'));
		$tahun		= date('Y');
		$kode_surat = "s_penelitian_$urut";
        $no_surat 	= "$urut/A.00/05/$bulan/$tahun"; //kode otomatis
        foreach ($mhs as $m) {
            $data = array(
                'no_surat' => $no_surat,
				'kode_surat'=> $kode_surat,
                'nim' => $m,
            );
            $this->m_penelitian->simpan_mhs($data); //insert
        }
        redirect('c_penelitian/form_penelitian/' . $kode_surat);
    }

    function form_penelitian ($no_surat) { //3
        $data['no_surat'] = $no_surat;
        $this->load->view('penelitian/form_penelitian', $data);
    }

    function simpan_penelitian() {//4
        $no_surat = $this->input->post('no_surat');
        $data = array(
                //tempat, tertuju,mulai, selesai
			'tempat_penelitian' => $this->input->post('tempat_penelitian'),
            'tertuju'      		=> $this->input->post('tertuju'),
            'tgl_mulai' 		=> $this->input->post('tgl_mulai'),
			'tgl_selesai' 		=> $this->input->post('tgl_selesai'),
        );
        $this->m_penelitian->edit_instansi_penelitian($data, $no_surat); //do model buat query edit;
        //edit tb_penelitian where no_surat = 
        redirect('c_penelitian/tb_penelitian/' . $no_surat);
    }

    function tb_penelitian() {//6
        //menmapilkan data penelitian dari mhassiwa yang login
		$data['penelitian']= $this->m_penelitian->tb_penelitian($this->session->userdata('id'));
        $this->load->view('penelitian/tb_penelitian', $data);
    }

    function cetak_penelitian($id_penelitian) { //5

        //model (buat query), jangan lipa perhatikan parameter
        //select * from tb_penelitian join tb_mahasiswa where kode_surat = $no_surat group by no_surat
        //select * from tb_penelitian join tb_mahasiswa where kode_surat = $no_surat (untuk nampilin tabel mhs)
        $data['penelitian'] = $this->m_penelitian->tampil_penelitian($id_penelitian);
		$data['penelitian2'] = $this->m_penelitian->tampil2($id_penelitian);
        // var_dump($data);

        $this->load->view('penelitian/cetak_penelitian', $data); //$data
    }
	function hapus_penelitian($id_penelitian){
		$this->m_penelitian->hapus_penelitian($id_penelitian);
		redirect('c_penelitian/tb_penelitian');
	}

}

?>