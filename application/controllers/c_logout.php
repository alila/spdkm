<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class c_logout extends CI_Controller {
	
	function index()
	{
		$this->session->sess_destroy();
		$this->session->unset_userdata(md5('$id'));
	    $this->session->unset_userdata('$nama');
		redirect('');
	}
}
?>