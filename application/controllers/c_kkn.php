<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class c_kkn extends CI_Controller{
	function __construct(){
		parent::__construct();
			if(!$this->session->userdata('id')){
			redirect('c_login');
		}
	}
	
	function tb_mhs_kkn() { //1
        //menampilkan data mahasiwa semua mahasiswa
        $data['mhs'] = $this->m_kkn->mhs();
        $this->load->view('kkn/tb_mhs_kkn', $data); //$data
    }
	 function simpan_mhs_kkn() { //2
//        insert data ke tb_penelitian
        $mhs 		= $this->input->post('Mhs[]');
		$urut 		= $this->m_kkn->urut();
		$bulan 		= $this->m_kkn->romawi(date('m'));
		$tahun		= date('Y');
		$kode_surat = "s_penelitian_$urut";
        $no_surat 	= "$urut/A.00/05/$bulan/$tahun"; //kode otomatis
        foreach ($mhs as $k) {
            $data = array(
                'no_surat' => $no_surat,
				'kode_surat'=> $kode_surat,
                'nim' => $k,
            );
            $this->m_kkn->simpan_mhs($data); //insert
        }
        redirect('c_kkn/form_kkn/' . $kode_surat);
    }

    function form_kkn($no_surat) { //3
        $data['no_surat'] = $no_surat;
        $this->load->view('kkn/form_kkn', $data);
    }

    function simpan_kkn() {//4
        $no_surat = $this->input->post('no_surat');
        $data = array(
                //tempat, tertuju,mulai, selesai
			'tempat_kkn' 	=> $this->input->post('tempat_kkn'),
            'tertuju'		=> $this->input->post('tertuju'),
            'tgl_mulai'		=> $this->input->post('tgl_mulai'),
			'tgl_selesai' 	=> $this->input->post('tgl_selesai'),
        );
        $this->m_kkn->edit_instansi_kkn($data, $no_surat); //do model buat query edit;
        //edit tb_penelitian where no_surat = 
        redirect('c_kkn/cetak_kkn/' . $no_surat);
    }
	function cetak_kkn($no_surat) { //5
        //model (buat query), jangan lipa perhatikan parameter
        //select * from tb_penelitian join tb_mahasiswa where kode_surat = $no_surat group by no_surat
        //select * from tb_penelitian join tb_mahasiswa where kode_surat = $no_surat (untuk nampilin tabel mhs)
		$data['kkn'] = $this->m_kkn->tampil_kkn($no_surat);
		$data['kkn'] = $this->m_kkn->tampil2($no_surat);

        $this->load->view('kkn/cetak_kkn', $data); //$data
    }

    function tb_kkn() {//6
        //menmapilkan data penelitian dari mhassiwa yang login
		$data['kkn']= $this->m_kkn->tampil_tb_kkn($this->session->userdata('id'));
        $this->load->view('kkn/tb_kkn', $data);
    }

   
	function hapus_kkn($id_kkn){
		$this->m_kkn->hapus_kkn($id_kkn);
		redirect('c_kkn/tb_kkn');
	}
	
}
?>