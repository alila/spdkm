<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {
	public function index()
	{
		$this->load->view('beranda');
	}
	function tables()
	{
		$this->load->view('tables');
	}
	function admin()
	{
		$this->load->view('admin');
	}
	function home()
	{
		$this->load->view('home');
	}
}
?>
