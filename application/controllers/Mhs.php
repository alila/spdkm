<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Mhs extends CI_Controller{
	
	function __construct(){
		parent::__construct();
			if(!$this->session->userdata('id')){
			redirect('c_login');
		}
	}
	function input_Mhs(){
		$this->load->view('mahasiswa/mahasiswa');
	}
	function card2(){
		$this->load->view('card2');
	}
	function home(){
		$this->load->view('home');
	}
	function index(){
		$nim=$this->session->userdata('id');
		$data['mhs'] = $this->Mhs_m->list_mhs($nim);
		$this->load->view('mahasiswa/list_mhs', $data);
	}
	function proses_input(){	
		//konfigurasi
		$nama_file = 'file-foto'.time();
		$config['upload_path']='./assets/foto/';	
		$config['allowed_types']='jpg|png|doc|pdf'; 
		$config['file_name']=$nama_file;
		$this->upload->initialize($config);
		//upload
		if($this->upload->do_upload('file-foto')){
			$foto = $this->upload->data();
			
		$data = array(
			'nim'=> $this->input->post('nim'),
			'nama'=> $this->input->post('nama'),
			'prodi'=> $this->input->post('prodi'),
			'tempat_lahir'=> $this->input->post('tempat_lahir'),
			'tgl_lahir'=> $this->input->post('tgl_lahir'),
			'nama_ortu'=> $this->input->post('nama_ortu'),
			'pekerjaan_ortu'=> $this->input->post('pekerjaan_ortu_ortu'),
			'alamat'=> $this->input->post('alamat'),
			'password'=> $this->input->post('password'),
			'foto_mhs'=>$foto['file_name']
			);
		$this->Mhs_m->proses_input_db($data);
		redirect('Mhs');	
		} else {
			$error = array('error'=>$this->upload->display_errors());
			echo json_encode($error);
		}	
	}
	function hapus_mhs($nim){
		$this->Mhs_m->hapus_mhs($nim);
		redirect('Mhs');
	}
	function edit_mhs($nim){
		$data['mhs'] = $this->Mhs_m->edit_mhs($nim);
		$this->load->view('mahasiswa/edit', $data);
	}
	function proses_edit(){
		$nama_file = 'file-foto'.time();
		$config['upload_path']='./assets/foto/';	
		$config['allowed_types']='jpg|png|doc|pdf'; 
		$config['file_name']=$nama_file;
		$this->upload->initialize($config);
		
		//upload
		if($this->upload->do_upload('file-foto')){
			$foto = $this->upload->data();
		$nim = $this->input->post('nim');
		$data = array(
			'nama'=> $this->input->post('nama'),
			'prodi'=> $this->input->post('prodi'),
			'tempat_lahir'=> $this->input->post('tempat_lahir'),
			'tgl_lahir'=> $this->input->post('tgl_lahir'),
			'nama_ortu'=> $this->input->post('nama_ortu'),
			'pekerjaan_ortu'=> $this->input->post('pekerjaan_ortu'),
			'alamat'=> $this->input->post('alamat'),
			'password'=> $this->input->post('password'),
			'foto_mhs'=>$foto['file_name']
			);
		$this->Mhs_m->proses_edit_db($data, $nim);
		redirect('Mhs');
		}else{
			$error=array(
			'error'=>$this->upload->display_errors());
			echo json_encode($error);
			
		}
	}
}

?>