<?php
class m_penelitian extends CI_Model{
	function simpan_mhs($data) {
        $this->db->insert('tb_penelitian', $data);
    }
	function tampil3($no_surat){
		$query = $this->db->query("select * from tb_penelitian join tb_mahasiswa on tb_penelitian.nim = tb_mahasiswa.nim where kode_surat = '$no_surat' ");
		return $query->result();
	}
	function tampil2($no_surat){
		$query = $this->db->query("select * from tb_penelitian join tb_mahasiswa on tb_penelitian.nim = tb_mahasiswa.nim where id_penelitian = '$no_surat' ");
		return $query->result();
	}
	function tampil_penelitian2($no_surat){
		$query = $this->db->query("select * from tb_penelitian join tb_mahasiswa on tb_penelitian.nim = tb_mahasiswa.nim 
		where kode_surat = '$no_surat' group by no_surat");
		return $query->result();
	}
	function tampil_penelitian($no_surat){
		$query = $this->db->query("select * from tb_penelitian join tb_mahasiswa on tb_penelitian.nim = tb_mahasiswa.nim 
		where id_penelitian = '$no_surat'");
		return $query->result();
	}
	 function edit_instansi_penelitian($data, $no_surat) {
        $this->db->where('kode_surat', $no_surat);
        $this->db->update('tb_penelitian', $data);
    }
	function simpan_penelitian_db($data){
		$this->db->insert('tb_penelitian', $data);
	}
	function mhs (){
		$query = $this->db->query("SELECT * FROM tb_mahasiswa");
		return $query->result();
	}

	function id_penelitian(){
		$query = $this->db->query('SELECT * FROM tb_penelitian');
		$baris = $query->num_rows();
		$urut = $baris +1;
		$kode = "ID_PLT_$urut";
		return $kode;
	}
	function proses_input_db($data){
		$this->db->insert('tb_penelitian', $data);
	}
	function list_penelitian(){
		$query = $this->db->query("SELECT * FROM tb_penelitian");
		return $query->result();
	}
	
	function hapus_penelitian($id_penelitian){
		$this->db->where('id_penelitian', $id_penelitian);
		$this->db->delete('tb_penelitian');
	}
	function edit_penelitian($no_surat){
		$query = $this->db->query("SELECT * FROM tb_penelitian WHERE no_surat = '$no_surat' ");
		return $query->result();
	}
	function proses_edit_db($data, $nim){
		$this->db->where('nim', $nim);
		$this->db->update('tb_penelitian', $data);
	}
	function ajukan_penelitian($nim){
		$query = $this->db->query("SELECT * FROM tb_penelitian where nim='$nim'");
		return $query->result();
	}
	function tb_penelitian($nim){
		$query = $this->db->query("SELECT * FROM tb_penelitian where nim='$nim' ");
		return $query->result();
	}
	//---------
	function penelitian($id_penelitian){
		$query = $this->db->query("SELECT * FROM tb_mahasiswa join tb_penelitian 
		on tb_mahasiswa.nim = tb_penelitian.nim WHERE tb_penelitian.id_penelitian = '$id_penelitian' ");
		return $query->result();
	}
	function instansi_penelitian($id_penelitian){
		$query = $this->db->query("SELECT * FROM tb_penelitian  WHERE id_penelitian = '$id_penelitian' ");
		return $query->result();
	}
	function mhs_penelitian ($nim){
		$query = $this->db->query("SELECT * FROM tb_mahasiswa  WHERE id_penelitian = '$nim' ");
		return $query->result();
	}
	function mhs_penelitian2 ($nim){
		$query = $this->db->query("SELECT * FROM tb_penelitian  WHERE nim = '$nim' ");
		return $query->result();
	}
	//---------
	function tgl($tgl){
		$hari = substr($tgl, 8, 2);
        $tahun = substr($tgl, 0, 4);
        $nama_bulan = $this->bulan($tgl);
        $tgl_oke = $hari . ' ' . $nama_bulan . ' ' . $tahun;
        return $tgl_oke;
	}
	function bulan($tgl){
		$bulan = substr($tgl, 5, 2);
        Switch ($bulan) {
            case 1 : $bulan = "Januari";
                Break;
            case 2 : $bulan = "Februari";
                Break;
            case 3 : $bulan = "Maret";
                Break;
            case 4 : $bulan = "April";
                Break;
            case 5 : $bulan = "Mei";
                Break;
            case 6 : $bulan = "Juni";
                Break;
            case 7 : $bulan = "Juli";
                Break;
            case 8 : $bulan = "Agustus";
                Break;
            case 9 : $bulan = "September";
                Break;
            case 10 : $bulan = "Oktober";
                Break;
            case 11 : $bulan = "November";
                Break;
            case 12 : $bulan = "Desember";
                Break;
        }
        return $bulan;
	}
	function urut(){
		$query = $this->db->query("SELECT * FROM tb_penelitian");
		$baris = $query->num_rows();
		$urut = $baris + 1;
		return $urut;
	}
	function romawi($m){
        Switch ($m) {
            case 1 : $bulan = "I";
                Break;
            case 2 : $bulan = "II";
                Break;
            case 3 : $bulan = "III";
                Break;
            case 4 : $bulan = "IV";
                Break;
            case 5 : $bulan = "V";
                Break;
            case 6 : $bulan = "VI";
                Break;
            case 7 : $bulan = "VII";
                Break;
            case 8 : $bulan = "VIII";
                Break;
            case 9 : $bulan = "IX";
                Break;
            case 10 : $bulan = "X";
                Break;
            case 11 : $bulan = "XI";
                Break;
            case 12 : $bulan = "XII";
                Break;
        }
        return $bulan;
	}
}
?>