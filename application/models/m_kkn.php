<?php
class m_kkn extends CI_Model{
	function simpan_mhs($data) {
        $this->db->insert('tb_kkn', $data);
    }
	 function edit_instansi_kkn($data, $no_surat) {
        $this->db->where('kode_surat', $no_surat);
        $this->db->update('tb_kkn', $data);
    }
	function tampil_kkn($no_surat){
		$query = $this->db->query("select * from tb_kkn join tb_mahasiswa on tb_kkn.nim = tb_mahasiswa.nim 
		where id_kkn = '$no_surat' group by no_surat");
		return $query->result();
	}
	function tampil2($no_surat){
		$query = $this->db->query("select * from tb_kkn join tb_mahasiswa on tb_kkn.nim = tb_mahasiswa.nim where id_kkn = '$no_surat' ");
		return $query->result();
	}
	function tampil_tb_kkn($nim){
		$query = $this->db->query("SELECT * FROM tb_kkn where nim='$nim' ");
		return $query->result();
	}
	function hapus_kkn($id_kkn){
		$this->db->where('id_kkn', $id_kkn);
		$this->db->delete('tb_kkn');
	}
	function id_kkn(){
		$query = $this->db->query('SELECT * FROM tb_kkn');
		$baris = $query->num_rows();
		$urut = $baris +1;
		$kode = "ID_KKN_$urut";
		return $kode;
	}
	function proses_input_db($data){
		$this->db->insert('tb_kkn', $data);
	}
	
	function edit_kkn($nim){
		$query = $this->db->query("SELECT * FROM tb_kkn WHERE nim='$nim'");
		return $query->result();
	}
	function proses_edit_db($data, $nim){
		$this->db->where('nim', $nim);
		$this->db->update('tb_kkn', $data);
	}
	function ajukan_kkn($nim){
		$query = $this->db->query("SELECT * FROM tb_kkn where nim='$nim' ");
		return $query->result();
	}
	function tb_kkn($nim){
		$query = $this->db->query("SELECT * FROM tb_kkn where nim='$nim' ");
		return $query->result();
	}
	//---------
	function kkn($id_kkn){
		$query = $this->db->query("SELECT * FROM tb_mahasiswa join tb_kkn 
		on tb_mahasiswa.nim = tb_kkn.nim WHERE tb_kkn.id_kkn = '$id_kkn' ");
		return $query->result();
	}
	function instansi_kkn($id_kkn){
		$query = $this->db->query("SELECT * FROM tb_kkn  WHERE id_kkn = '$id_kkn' ");
		return $query->result();
	}
	function mhs (){
		$query = $this->db->query("SELECT * FROM tb_mahasiswa");
		return $query->result();
	}
	//---------
	function tgl($tgl){
		$hari = substr($tgl, 8, 2);
        $tahun = substr($tgl, 0, 4);
        $nama_bulan = $this->bulan($tgl);
        $tgl_oke = $hari . ' ' . $nama_bulan . ' ' . $tahun;
        return $tgl_oke;
	}
	function bulan($tgl){
		$bulan = substr($tgl, 5, 2);
        Switch ($bulan) {
            case 1 : $bulan = "Januari";
                Break;
            case 2 : $bulan = "Februari";
                Break;
            case 3 : $bulan = "Maret";
                Break;
            case 4 : $bulan = "April";
                Break;
            case 5 : $bulan = "Mei";
                Break;
            case 6 : $bulan = "Juni";
                Break;
            case 7 : $bulan = "Juli";
                Break;
            case 8 : $bulan = "Agustus";
                Break;
            case 9 : $bulan = "September";
                Break;
            case 10 : $bulan = "Oktober";
                Break;
            case 11 : $bulan = "November";
                Break;
            case 12 : $bulan = "Desember";
                Break;
        }
        return $bulan;
	}
	function urut(){
		$query = $this->db->query("SELECT * FROM tb_kkn");
		$baris = $query->num_rows();
		$urut = $baris + 1;
		return $urut;
	}
	function romawi($m){
        Switch ($m) {
            case 1 : $bulan = "I";
                Break;
            case 2 : $bulan = "II";
                Break;
            case 3 : $bulan = "III";
                Break;
            case 4 : $bulan = "IV";
                Break;
            case 5 : $bulan = "V";
                Break;
            case 6 : $bulan = "VI";
                Break;
            case 7 : $bulan = "VII";
                Break;
            case 8 : $bulan = "VIII";
                Break;
            case 9 : $bulan = "IX";
                Break;
            case 10 : $bulan = "X";
                Break;
            case 11 : $bulan = "XI";
                Break;
            case 12 : $bulan = "XII";
                Break;
        }
        return $bulan;
	}
	
}
?>