<?php
class M_dok extends CI_Model{
	function data_kota(){
		//select from mahasiswa
		$query= $this->db->query("select *from kota  ");
		return $query->result();
	}
	//MODEL SURAT AKTIF
	function simpan_aktif($data){
		$this->db->insert('tb_aktif', $data);
	}
	//MODEL SURAT MAGANG
	function id_magang(){
		$query = $this->db->query('SELECT * FROM tb_magang');
		$baris = $query->num_rows();
		$urut = $baris +1;
		$kode = "dok_magang_$urut";
		return $kode;
	}
	function simpan_magang_db($data){
		$this->db->insert('tb_mhs_magang', $data);
	}
	function update_mhs_magang($data, $m){
		$this->db->where('nim', $m);
		$this->db->update('tb_mahasiswa', $data);
		
	}
	//MODEL SURAT PENELITIAN
	function id_penelitian(){
		$query = $this->db->query('SELECT * FROM tb_penelitian');
		$baris = $query->num_rows();
		$urut = $baris +1;
		$kode = "ID_PLT_$urut";
		return $kode;
	}	
	function simpan_penelitian_db($data){
		$this->db->insert('tb_penelitian', $data);
	}
	function update_mhs_penelitian($data, $p){
		$this->db->where('nim', $p);
		$this->db->update('mahasiswa', $data);
		
	}
	//MODEL SURAT KKN
	function id_kkn(){
		$query = $this->db->query('SELECT * FROM tb_kkn');
		$baris = $query->num_rows();
		$urut = $baris +1;
		$kode = "dok_kkn_$urut";
		return $kode;
	}	
	function simpan_kkn_db($data){
		$this->db->insert('tb_kkn', $data);
	}
	function update_mhs_kkn($data, $k){
		$this->db->where('nim', $k);
		$this->db->update('mahasiswa', $data);
		
	}
}
?>