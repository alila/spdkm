<?php
class m_magang extends CI_Model{
	function data_dosbing(){
		$query= $this->db->query("select * from tb_pegawai  ");
		return $query->result();
	}
	function id_magang(){
		$query = $this->db->query('SELECT * FROM tb_magang');
		$baris = $query->num_rows();
		$urut = $baris +1;
		$kode = "ID_MG_$urut";
		return $kode;
	}
	function kode_form(){
		$query = $this->db->query('SELECT * FROM tb_magang');
		$baris = $query->num_rows();
		$urut = $baris +1;
		$kode = "FRM_MG_$urut";
		return $kode;
	}

	function simpan_magang_db($data){
		$this->db->insert('tb_magang', $data);
	}
	
	function simpan_form_ijin_magang($id,$data){
		$this->db->where('id', $id);
		$this->db->update('tb_mhs_magang', $data);
	}
	
	function proses_input_db($data){
		$this->db->insert('tb_magang', $data);
	}
	function list_magang($nim){
		$query = $this->db->query("SELECT * FROM tb_magang WHERE nim='$nim' ");
		return $query->result();
	}
	function edit_magang($nim){
		$query = $this->db->query("SELECT * FROM tb_magang WHERE nim='$nim' ");
		return $query->result();
	}
	function proses_edit_db($data, $nim){
		$this->db->where('nim', $nim);
		$this->db->update('tb_magang', $data);
	}
	function ajukan_magang($nim){
		$query = $this->db->query("SELECT * FROM tb_magang where nim='$nim' ");
		return $query->result();
	}
	//function magang($nim){
		//$query = $this->db->query("SELECT * FROM tb_mahasiswa join tb_magang on tb_mahasiswa.nim = tb_magang.nim WHERE nim = '$nim' ");
		//return $query->result();
	//}
	function magang2($id_magang){
		$query = $this->db->query("SELECT * FROM tb_mahasiswa join tb_mhs_magang 
		on tb_mahasiswa.nim = tb_mhs_magang.nim WHERE tb_mhs_magang.id_magang = '$id_magang' ");
		return $query->result();
	}
	function biodata_magang($id_magang){
		$query = $this->db->query("SELECT * FROM tb_mahasiswa join tb_mhs_magang 
		on tb_mahasiswa.nim = tb_mhs_magang.nim WHERE tb_mhs_magang.id_magang = '$id_magang' ");
		return $query->result();
	}
	function ijin_magang($nim){
		$query = $this->db->query("select * from tb_mahasiswa 
		join tb_mhs_magang on tb_mahasiswa.nim = tb_mhs_magang.nim 
		join tb_magang on tb_magang.id_magang=tb_mhs_magang.id_magang 
		where tb_mahasiswa.nim='$nim' ");
		return $query->result();
	}
	function instansi($id_magang){
		$query = $this->db->query("SELECT * FROM tb_magang  WHERE id_magang = '$id_magang' ");
		return $query->result();
	}
	function mhs_biodata ($nim){
		$query = $this->db->query("SELECT * FROM tb_mahasiswa  WHERE nim = '$nim' ");
		return $query->result();
	}
	function mhs ($id_magang){
		$query = $this->db->query("SELECT * FROM tb_mahasiswa ");
		return $query->result();
	}
	//3/8/2018--------
	
	
	//---------
	function tgl($tgl){
		$hari = substr($tgl, 8, 2);
        $tahun = substr($tgl, 0, 4);
        $nama_bulan = $this->bulan($tgl);
        $tgl_oke = $hari . ' ' . $nama_bulan . ' ' . $tahun;
        return $tgl_oke;
	}
	function bulan($tgl){
		$bulan = substr($tgl, 5, 2);
        Switch ($bulan) {
            case 1 : $bulan = "Januari";
                Break;
            case 2 : $bulan = "Februari";
                Break;
            case 3 : $bulan = "Maret";
                Break;
            case 4 : $bulan = "April";
                Break;
            case 5 : $bulan = "Mei";
                Break;
            case 6 : $bulan = "Juni";
                Break;
            case 7 : $bulan = "Juli";
                Break;
            case 8 : $bulan = "Agustus";
                Break;
            case 9 : $bulan = "September";
                Break;
            case 10 : $bulan = "Oktober";
                Break;
            case 11 : $bulan = "November";
                Break;
            case 12 : $bulan = "Desember";
                Break;
        }
        return $bulan;
	}
	function urut(){
		$query = $this->db->query("SELECT * FROM tb_magang");
		$baris = $query->num_rows();
		$urut = $baris + 1;
		return $urut;
	}
	function romawi($m){
        Switch ($m) {
            case 1 : $bulan = "I";
                Break;
            case 2 : $bulan = "II";
                Break;
            case 3 : $bulan = "III";
                Break;
            case 4 : $bulan = "IV";
                Break;
            case 5 : $bulan = "V";
                Break;
            case 6 : $bulan = "VI";
                Break;
            case 7 : $bulan = "VII";
                Break;
            case 8 : $bulan = "VIII";
                Break;
            case 9 : $bulan = "IX";
                Break;
            case 10 : $bulan = "X";
                Break;
            case 11 : $bulan = "XI";
                Break;
            case 12 : $bulan = "XII";
                Break;
        }
        return $bulan;
	}
	
	
}
?>