 <?php
class m_aktif extends CI_Model{
	function proses_input_db($data){
		$this->db->insert('tb_aktif', $data);
	}
	function ajukan_aktif($nim){
		$query = $this->db->query("SELECT * FROM tb_aktif where nim='$nim' ");
		return $query->result();
	}
	function hapus_aktif($id_aktif){
		$this->db->where('id_aktif', $id_aktif);
		$this->db->delete('tb_aktif');
	}
	
	function edit_aktif($nim){
		$query = $this->db->query("SELECT * FROM tb_aktif WHERE nim='$nim' ");
		return $query->result();
	}
	function proses_edit_aktif_db($data, $nim){
		$this->db->where('nim', $nim);
		$this->db->update('tb_aktif', $data);
	}
	
	function aktif($id_aktif){
		$query = $this->db->query("SELECT * FROM tb_mahasiswa join tb_aktif on tb_mahasiswa.nim = tb_aktif.nim WHERE id_aktif = '$id_aktif' ");
		return $query->result();
	}
	
	function tgl($tgl){
		$hari = substr($tgl, 8, 2);
        $tahun = substr($tgl, 0, 4);
        $nama_bulan = $this->bulan($tgl);
        $tgl_oke = $hari . ' ' . $nama_bulan . ' ' . $tahun;
        return $tgl_oke;
	}
	function bulan($tgl){
		$bulan = substr($tgl, 5, 2);
        Switch ($bulan) {
            case 1 : $bulan = "Januari";
                Break;
            case 2 : $bulan = "Februari";
                Break;
            case 3 : $bulan = "Maret";
                Break;
            case 4 : $bulan = "April";
                Break;
            case 5 : $bulan = "Mei";
                Break;
            case 6 : $bulan = "Juni";
                Break;
            case 7 : $bulan = "Juli";
                Break;
            case 8 : $bulan = "Agustus";
                Break;
            case 9 : $bulan = "September";
                Break;
            case 10 : $bulan = "Oktober";
                Break;
            case 11 : $bulan = "November";
                Break;
            case 12 : $bulan = "Desember";
                Break;
        }
        return $bulan;
	}
	
	//03-08-18
	function urut(){
		$query = $this->db->query("SELECT * FROM tb_aktif");
		$baris = $query->num_rows();
		$urut = $baris + 1;
		return $urut;
	}
	function romawi($m){
        Switch ($m) {
            case 1 : $bulan = "I";
                Break;
            case 2 : $bulan = "II";
                Break;
            case 3 : $bulan = "III";
                Break;
            case 4 : $bulan = "IV";
                Break;
            case 5 : $bulan = "V";
                Break;
            case 6 : $bulan = "VI";
                Break;
            case 7 : $bulan = "VII";
                Break;
            case 8 : $bulan = "VIII";
                Break;
            case 9 : $bulan = "IX";
                Break;
            case 10 : $bulan = "X";
                Break;
            case 11 : $bulan = "XI";
                Break;
            case 12 : $bulan = "XII";
                Break;
        }
        return $bulan;
	}
	function smt(){
		$now = date('n');
		$th_now = date('Y');
		$th_next = date ('Y', strtotime('+1 years'));
		$th_before = date ('Y', strtotime('-1 years'));
		switch ($now){
			case 9;
			case 10;
			case 11;
			case 12;
			case 1;
			case 2;
		$bulan = "Semester Ganjil tahun akademik $th_now/$th_next";
		break;
			case 3;
			case 4;
			case 5;
			case 6;
			case 7;
			case 8;
		$bulan = "Semester Genap tahun akademik $th_before/$th_now";
		break;
		} return $bulan;
	}
	
	
	
}
?>