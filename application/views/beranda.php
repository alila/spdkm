<?php $this->load->view('header_br');?>

        <!-- Page Content -->
	<section id="about">
   
	  <h2 class="my-4">About Us</h2>
      <!-- Marketing Icons Section -->
      <div class="row">
        <div class="col-lg-4 mb-4">
          <div class="card h-100">
            <h4 class="card-header">Visi</h4>
            <div class="card-body">
              <p class="card-text">Tahun 2025 menjadi Perguruan Tinggi dengan proyeksi menghadirkan lulusan Bidang Teknologi Informasi dan Komputer berkualitas yang unggul dalam bidang informatika bisnis.</p>
            </div>
          </div>
        </div>
        <div class="col-lg-4 mb-4">
          <div class="card h-100">
            <h4 class="card-header">Misi</h4>
            <div class="card-body">
              <p class="card-text">
				<ol type="1">
					<li>Menyelenggarakan pendidikan tinggi bidang sistem teknologi informasi yang berkualitas dan berteknologi unggul, yang dikelola secara professional dengan menjunjung tinggi nilai-nilai ketaqwaan serta menggunakan pola piker global dan sistem manajemen berkualitas.</li>
					<li>Membentuk insan akademik yang bertanggung jawab dan berwawasan kebangsaan.</li>
					<li>Menyelenggarakan penelitian, pengabdian masyarakat dan kerjasama yang luas dengan pemangku kepentingan.</li>
				</ol>
			</p>
            </div>
          </div>
        </div>
        <div class="col-lg-4 mb-4">
          <div class="card h-100">
            <h4 class="card-header">Tujuan</h4>
            <div class="card-body">
              <p class="card-text">Universitas Jenderal Achmad Yani Yogyakarta menyelenggarakan berbagai Program Studi unggulan menyiapkan lulusan yang Unggul dan mewarisi kejuangan Jenderal Achmad Yani.</p>
            </div>
          </div>
        </div>
      </div>
      <!-- /.row -->

      <!-- Features Section -->
	  <section id="contact">
	  <div class="container">
      <div class="row">
        <div class="col-lg-6">
          <h2>Contact</h2>
          <ul><p>Come and Follow us:</p>
            <li>Facebook</li>
            <li>Instagram</li>
            <li>Twitter</li>
          </ul>
        </div>
      </div>
      <!-- /.row -->

      <hr>

      <!-- Call to Action Section -->
      

    </div>
    <!-- /.container -->
  
  <?php $this->load->view('footer_br');?>  
  
