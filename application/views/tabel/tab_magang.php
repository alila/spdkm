<?php $this->load->view('./header');?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
       <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="<?php echo site_url('Mhs/home');?>">Dashboard</a>
        </li>
       </ol> 
	  
      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i> Data Tabel Surat Pengantar Magang Kerja Anda</div>
        <div class="card-body">
          <div class="table-responsive">
		  <a href='<?php echo site_url('c_dok/s_magang');?>' class="btn btn-primary btn-block">Tambah</a>

            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>No</th>
                  <th>NIM</th>
				  <th>Nama</th>
                  <th>Nama Instansi</th>
				  <th>Keterangan</th>
				  <th>Tanggal Mulai</th>
				  <th>Tanggal Selesai</th>
				  <th>Action</th>
                </tr>
              </thead>
              <tbody>
				<?php 
				$no=1;
				foreach($c_dokumen as $sm){?>
					<tr>
						<td><?php echo $no++; ?></td>
						<td><?php echo $sm->nim ?></td>
						<td><?php echo $sm->nama ?></td>
						<td><?php echo $sm->nama_instansi ?></td>
						<td><?php echo $sm->keterangan ?></td>
						<td><?php echo $sm->mulai ?></td>
						<td><?php echo $sm->selesai ?></td>
						<td>
							<a href="<?php echo site_url('c_dok/cetak_magang/'. $sm->id_dok_magang)?>" class="btn-sm btn-success">Print</a>
						</td>
					</tr>
                <?php }?>
              </tbody>
            </table>
          </div>
        </div>
        <!-- <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>-->
      </div>
    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
<?php $this->load->view('./footer');?>
    