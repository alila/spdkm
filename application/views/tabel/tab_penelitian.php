<?php $this->load->view('./header');?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
       <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="<?php echo site_url('Mhs/home');?>">Dashboard</a>
        </li>
       </ol> 
	  
      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i> Data Tabel Surat Pengantar Penelitian</div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>No</th>
                  <th>No Surat</th>
				  <th>Tujuan</th>
				  <th>Tanggal Mulai</th>
				  <th>Tanggal Selesai</th>
				  <th>Keterangan</th>
				  <th>Action</th>
                </tr>
              </thead>
              <tbody>
				<?php 
				$no=1;
				foreach($c_dokumen as $p){?>
					<tr>
					  <td><?php echo $no++; ?></td>
					  <td><?php echo $p->no_surat ?></td>
					  <td><?php echo $p->tujuan ?></td>
					  <td><?php echo $p->mulai ?></td>
					  <td><?php echo $p->selesai ?></td>
					  <td><?php echo $p->ket ?></td>
					  <td>
						<a href="<?php echo site_url('c_dok/cetak_penelitian/'. $p->id_dok_penelitian)?>" class="btn-sm btn-success">Print</a>
						</td>
					</tr>
                <?php }?>
              </tbody>
            </table>
          </div>
        </div>
        <!-- <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>-->
      </div>
    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
<?php $this->load->view('./footer');?>
    