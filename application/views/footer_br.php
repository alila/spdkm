 <!-- Footer -->
    <footer class="py-5 bg">
      <div class="container">
        <p class="m-0 text-center text-white">Copyright &copy; SPDKM UNJAYA Yogyakarta 2018</p>
      </div>
      <!-- /.container -->
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="<?php echo base_url();?>/assets/beranda/vendor/jquery/jquery.min.js"></script>
    <script src="<?php echo base_url();?>/assets/beranda/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
