<?php foreach ($c_dokumen as $c){
?>
<html>
<head>
<link href="<?php echo base_url();?>/assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template-->
<link href="<?php echo base_url();?>/assets/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
</head>

<body>
<a title="Close" href="<?php echo site_url('c_dok');?>"> 
	<i class="fa fa-window-close" ></i> Close  
</a>

<table border="1" align="center" width="80%" height="900">
<tr><td valign="top">

<table  align="center" width="90%" border="0">
<tr><br><br>
	<td align="center" valign="top" colspan="3">

		<br><br>
		<h3><b><u>SURAT KETERANGAN</u></b><br></h3>
		<b>Nomor :	<?php echo $c->no_surat; ?></b></td>

<tr>
	<td colspan="3">
	<table width="100%">

	
	<tr></tr>
	<br>
	<tr><td colspan="3">Dekan Fakultas Teknik dan Teknologi Informasi Universitas Jenderal Achmad Yani Yogyakarta menerangkan bahwa:	<br><br>	
	<tr><td>Nama</td><td>: 					<?php echo $c->nama; ?></td></tr>
	<tr><td>Tempat Tanggal Lahir</td><td>: 	<?php echo $c->tempat_lahir; ?>, <?php echo $this->m_aktif->tgl($c->tgl_lahir); ?></td></tr>
	<tr><td>N.I.M</td><td>: 				<?php echo $c->nim; ?></td></tr>
	<tr><td>Nama Orang Tua</td><td>: 		<?php echo $c->nama_ortu; ?></td></tr>
	<tr><td>Pekerjaan Orang Tua</td><td>:	<?php echo $c->pekerjaan_ortu; ?></td></tr>
	<tr><td>Alamat</td><td>: 				<?php echo $c->alamat; ?></td></tr>
	<tr><td colspan="3"><br/>adalah benar-benar terdaftar sebagai mahasiswa aktif program studi <?php echo $c->prodi; ?> 
	Fakultas Teknik dan Teknologi Informasi Universitas Jenderal Achmad Yani, pada <?php echo $this->m_aktif->smt();?>. Surat keterangan ini dibuat agar dapat dipergunakan seperlunya.<br/><br/>	
<tr>
	<td colspan="2"></td><td width="290">
	Yogyakarta, <?php echo $this->m_aktif->tgl(date("Y/m/d"));?></br>
	A.n Dekan,<br>
	Fakultas Teknik dan Teknologi Informasi<br>
	Ka.Bag. Administrasi Akademik dan<br>
	Kemahasiswaan<br><br><br><br />
	
	
	Indawati, S.E., M.Ak.
	</td>
</tr>
</tr>
</table>

</td></tr>
</table>
<a class="float" title="cetak halaman" onclick="cetak()"> 
	<i class="fa fa-print my-float" ></i>   
</a>
</html>
<?php }?> 

<script>
function cetak(){
	print();
}
</script>
<style>
    @media print
    {    
        .float, .float *, .tombol
        {
            display: none !important;
        }
    }
    .float {
        position: fixed;
        width: 80px;
        height: 60px;
        bottom: 40px;
        right: 40px;
        background-color: Burlywood;
        color: #FFF;
        border-radius: 50px;
        text-align: center;
        box-shadow: 2px 2px 3px #999;
    }

    .my-float {
        margin-top: 22px;
    }
</style>
