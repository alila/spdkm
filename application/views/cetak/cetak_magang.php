<?php foreach ($c_dokumen as $m){
?>
<html>
<head>
<link href="<?php echo base_url();?>/assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template-->
<link href="<?php echo base_url();?>/assets/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
</head>

<body>

<a title="Close" href="<?php echo site_url('c_dok/data_magang_mhs');?>"> 
	<i class="fa fa-window-close" ></i> Close  
</a>

<table border="1" align="center" width="80%" height="900">
<tr><td valign="top">

<table  align="center" width="90%" border="0">
<tr>
	<td align="center" valign="top" colspan="3">
	<img src="<?php echo base_url('assets/foto/logo.jpg');?>" width="120px" height="120px"  style ="float:left; margin-bottom: 10px;"/>
	   <h4>UNIVERSITAS JENDERAL ACHMAD YANI YOGYAKARTA</h4>
	</td>
		<br>
</tr>
		<td align="right"><?php echo $this->m_magang->tgl(date("Y/m/d"));?></td>
		<tr>
			<td align="left">
				No. 	:	<?php echo $m->no_surat; ?><br>
				Lamp. 	: <br>
				Hal 	: <b><i><u>Ijin melaksanakan magang kerja</u></i></b>
			</td>
		</tr>
		<tr>
			<td align="left"><b>Kepada :<br>
				Yth. <br>
				<?php echo $m->nama_instansi; ?><br>
				Di_ <br>
				Tempat.</b>
			</td>
		</tr>
		
			
		
<tr>
	<td colspan="2">
	<table width="100%">

	
	<tr></tr>
	<br>
	<td colspan="3">Dengan hormat,<br>
	Bersama surat ini kami sampaikan semoga Bapak/Ibu dalam menjalankan tugas selalu dalam petunjuk dan bimbingan Tuhan Yang Maha Esa.<br>
	Dalam rangka menyelesaikan salah satu tugas mahasiswa di Universitas Jenderal Achmad Yani, maka kami mengharapkan kesediaan Bapak/Ibu untuk mengijinkan dan menerima mahasiswa kami:
	<table border=1 width="100%">
	<tr align='center'>
		<td>NO</td>
		<td>NIM</td>
		<td>NAMA</td>
		<td>PRODI</td>
	</tr>
	<?php 
		$no=1;
		foreach($c_dokumen1 as $m){
	?>
	<tr align='center'>
		<td><?php echo $no++; ?></td>
		<td><?php echo $m->nama; ?></td>
		<td><?php echo $m->nim; ?></td>
		<td><?php echo $m->prodi; ?></td>
	</tr>
		<?php }?>	
	</table>
	<tr><td colspan="3"><br/>Guna melaksanakan magang kerja di <!-- <?php echo $m->nama_instansi; ?> --> yang bapak/ibu pimpin, dengan pelaksanaan kerja sekurang-kurangnya 1 (satu) bulan.
	Adapun pelaksanaan magang mengikuti kesepakatan dengan mahasiswa.<br/>
	Sebagai bahan pertimbangan berikut kami lampirkan Panduan Ringkas Magang Kerja Mahasiswa.<br/>
	Selanjutnya demi kelancaran administrasi, kami mohon Bapak/Ibu berkenan memberikan tanggapan atas permohonan ini melalui Pj Humas & Kerjasama (Bpk Anton Basuki) melalui surat atau telpon di nomor (0274) 552489, 552851 atau di hotline 085101861996 Fax.(0274)557228.<br>
	Demikian atas perhatian dan perkenannya diucapkan terimakasih.
<tr>
	<td colspan="2"></td><td width="208">
	A.n Dekan FTTI,<br>
	Ka. Prodi Teknik Informatika<br><br><br /><br />
	
	
	Andika Bayu Saputra, S.Kom.,M.Kom.
	</td>
</tr>
</tr>
</table>

</td></tr>
</table>


<a class="float" title="cetak halaman" onclick="cetak()"> 
	<i class="fa fa-print my-float" ></i>   
</a>
</html>
<?php }?> 

<script>
function cetak(){
	print();
}
</script>
<style>
    @media print
    {    
        .float, .float *, .tombol
        {
            display: none !important;
        }
    }
    .float {
        position: fixed;
        width: 80px;
        height: 60px;
        bottom: 40px;
        right: 40px;
        background-color: Burlywood;
        color: #FFF;
        border-radius: 50px;
        text-align: center;
        box-shadow: 2px 2px 3px #999;
    }

    .my-float {
        margin-top: 22px;
    }
</style>
