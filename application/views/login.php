<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>Halaman Login Mahasiswa</title>
  <!-- Bootstrap core CSS-->
  <link href="<?php echo base_url();?>/assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="<?php echo base_url();?>/assets/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Custom styles for this template-->
  <link href="<?php echo base_url();?>/assets/css/sb-admin.css" rel="stylesheet">
	<style>
		.bg{
			background: SeaGreen;
			}
	</style>
</head>

<body class="bg">
  <div class="container">
    <div class="card card-login mx-auto mt-5">
      <div class="card-header" align='center'><h2>Login Mahasiswa</h2></div><br>
      <div class="card-body">
	  
	  <?php if ($this->session->flashdata('pesan_error')<> ''){ ?>
		<div class='alert alert-dismissible alert-danger'>  
			<?php echo $this->session->flashdata('pesan_error');?>
		</div>
	  <?php } ?>
	  
        <form method="post" action="<?php echo site_url('c_login/cek');?>">
          <div class="form-group">
            <label for="nim">N.I.M</label>
			
            <input name="nim" class="form-control" id="nim" type="text" aria-describedby="emailHelp">
          
		  </div>
          <div class="form-group">
            <label for="exampleInputPassword1">Password</label>
            <input name="pass" class="form-control" id="exampleInputPassword1" type="password">
          </div>
          <button type="submit" class="btn btn-primary btn-block" href="index.html">Login</button>
        </form>
      </div>
    </div>
  </div>
  <!-- Bootstrap core JavaScript-->
  <script src="<?php echo base_url();?>/assets/vendor/jquery/jquery.min.js"></script>
  <script src="<?php echo base_url();?>/assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <!-- Core plugin JavaScript-->
  <script src="<?php echo base_url();?>/assets/vendor/jquery-easing/jquery.easing.min.js"></script>
</body>

</html>
