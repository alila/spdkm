<?php $this->load->view('./header');
foreach($c_dok as $sa){
?>
<body class="bg-dark">
  <div class="container">
    <div class="card card-register mx-auto mt-5">
      <div class="card-header">Form Edit Data Surat Keterangan Aktif Kuliah</div>
      <div class="card-body">
        <form action="<?php echo site_url('c_dok/proses_edit_aktif')?>" method="post">
          <div class="form-group">
            <div class="form-row">
			<div class="col-md-6">
                <label for="exampleInputNIM">ID</label>
                <input value="<?php echo $sa->id_dok_aktif ?>" name ="id_dok_surat" class="form-control" id="id" type="text" aria-describedby="nameHelp">
              </div>
              <div class="col-md-6">
                <label for="exampleInputNIM">NIM</label>
                <input value="<?php echo $sa->nim ?>" name ="nim" class="form-control" id="NIM" type="text" aria-describedby="nameHelp" readonly="">
              </div>
              <div class="col-md-6">
                <label for="exampleInputName">No Surat</label>
                <input value="<?php echo $sa->no_surat ?>" name ="no_surat" class="form-control" id="nama" type="text" aria-describedby="nameHelp">
              </div>
			  <div class="col-md-6">
                <label for="exampleInputProdi">Keterangan</label>
                <input value="<?php echo $sa->keterangan ?> name ="keterangan" class="form-control" id="keterangan" type="text" aria-describedby="nameHelp">
              </div>
			  <div class="col-md-6">
                <label for="tgl">Tanggal</label>
                <input value="<?php echo $sa->tgl ?> name ="tgl" class="form-control" id="tgl" type="text" aria-describedby="nameHelp">
              </div>
            </div>
          </div> 
          <button type="submit" class="btn btn-primary btn-block">Simpan</button>
        </form>
      </div>
    </div>
  </div>
   <?php 
	}
   $this->load->view('./footer');
   ?>
