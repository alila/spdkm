<?php $this->load->view('./header')?>
 
<body class="bg-dark">
  <div class="container">
    <div class="card card-register mx-auto mt-5">
      <div class="card-header">Form Pengajuan <br>
	  Surat Pengantar Magang Kerja</div>
      <div class="card-body">
        <form action="<?php echo site_url('c_dok/ajukan_magang')?>" method="post" enctype="multipart/form-data">
          <div class="form-group">
            <div class="form-row">
			  <div class="col-md-6">
                <label for="NamaInstansi">Nama Instansi</label>
                <input name ="nama_instansi" class="form-control" id="exampleInputNamaInstansi" type="text" aria-describedby="nameHelp">
              </div>
              <div class="col-md-6">
                <label for="NIM">NIM</label>
                <input name ="nim" class="form-control" id="exampleInputNIM" type="text" aria-describedby="nameHelp">
              </div>
			  <div class="col-md-6">
			 <div class="form-group input-append date form_datetime">
                        <label for="exampleInputEmail1">Tanggal Mulai</label>
                        <input class="form-control"  type="text" value="" name="tgl_mulai" readonly>
                        <span class="add-on"><i class="icon-th"></i></span>
                    </div> </div>
			<div class="col-md-6">		
			<div class="form-group input-append date form_datetime">
                        <label for="exampleInputEmail1">Tanggal Selesai</label>
                        <input class="form-control"  type="text" value="" name="tgl_selesai" readonly>
                        <span class="add-on"><i class="icon-th"></i></span>
                    </div> </div>
			 
            </div>
          </div> 
			<button type="submit" class="btn btn-primary btn-block">Pengajuan</button>
			<div class="btn btn-warning btn-block"><a href="<?php echo site_url('Mhs/home')?>">Home</a></div>
        </form>   
      </div>
    </div>
  </div>
  
  <?php $this->load->view('./footer')?>
   
