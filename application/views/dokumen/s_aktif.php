<?php $this->load->view('./header')?>
   
<body class="bg-dark">
  <div class="container">
    <div class="card card-register mx-auto mt-5">
      <div class="card-header">Form Daftar<br>
	   Surat Keterangan Aktif Kuliah</div>
      <div class="card-body">
        <form action="<?php echo site_url('c_dok/ajukan_aktif')?>" method="post" enctype="multipart/form-data">
          <div class="form-group">
            <div class="form-row">
			  <div class="col-md-6">
			  <label for="keperluan">Keperluan</label>
                <textarea name ="keperluan" class="form-control" id="exampleInputketerangan" aria-describedby="nameHelp">
                </textarea>
              </div>
			  
            </div>
          </div> 
          <button type="submit" class="btn btn-primary btn-block">Pengajuan</button>
		   <div class="btn btn-warning btn-block"><a href="<?php echo site_url('Mhs/home')?>">Home</a></div>
        </form>   
      </div>
    </div>
  </div>
  
  <?php $this->load->view('./footer')?>
   
 
