<?php $this->load->view('./header');
foreach($mhs as $m){
?>
<body class="bg-dark">
  <div class="container">
    <div class="card card-register mx-auto mt-5">
      <div class="card-header">Form Data Mahasiswa</div>
      <div class="card-body">
        <form action="<?php echo site_url('Mhs/proses_edit')?>" enctype='multipart/form-data' method="post">
          <div class="form-group">
            <div class="form-row">
              <div class="col-md-6">
                <label for="exampleInputNIM">NIM</label>
                <input value="<?php echo $m->nim ?>" name ="nim" class="form-control" id="exampleInputNIM" type="text" aria-describedby="nameHelp" readonly="">
              </div>
              <div class="col-md-6">
                <label for="exampleInputName">Nama</label>
                <input value="<?php echo $m->nama ?>" name ="nama" class="form-control" id="exampleInputNama" type="text" aria-describedby="nameHelp">
              </div>
			  <div class="col-md-6">
                <label for="exampleInputProdi">Prodi</label>
                <input value="<?php echo $m->prodi ?>" name ="prodi" class="form-control" id="exampleInputProdi" type="text" aria-describedby="nameHelp">
              </div>
			  <div class="col-md-6">
                <label for="tempat_lahir">Tempat Lahir</label>
                <input value="<?php echo $m->tempat_lahir ?>" name ="tempat_lahir" class="form-control" id="exampleInputtgl_lahir" type="text" aria-describedby="nameHelp">
              </div>
			  <div class="col-md-6">
                <label for="tgl_lahir">Tanggal Lahir</label>
                <input value="<?php echo $m->tgl_lahir ?>" name ="tgl_lahir" class="form-control" id="exampleInputtgl_lahir" type="text" aria-describedby="nameHelp">
              </div>
			   <div class="col-md-6">
                <label for="nama_ortu">Nama Orang Tua</label>
                <input value="<?php echo $m->nama_ortu ?>" name ="nama_ortu" class="form-control" id="exampleInputpek_ortu" type="text" aria-describedby="nameHelp">
              </div>
			  <div class="col-md-6">
                <label for="pekerjaan_ortu">Pekerjaan Orang Tua</label>
                <input value="<?php echo $m->pekerjaan_ortu ?>" name ="pekerjaan_ortu" class="form-control" id="exampleInputpek_ortu" type="text" aria-describedby="nameHelp">
              </div>
			  <div class="col-md-6">
                <label for="exampleInputalamat">Alamat</label>
                <input value="<?php echo $m->alamat ?>" name ="alamat" class="form-control" id="exampleInputalamat" type="text" aria-describedby="nameHelp">
              </div>
			  <div class="col-md-6">
                <label for="exampleInputpek_ortu">Password</label>
                <input value="<?php echo $m->password ?>" name ="password" class="form-control" id="exampleInputpek_ortu" type="text" aria-describedby="nameHelp">
              </div>
			<div class="col-md-6">
                <label for="exampleInputpek_ortu">Foto</label>
                <input value="<?php echo $m->foto_mhs ?>" name = "file-foto" class="form-control" id="exampleInputpek_ortu" type="file" aria-describedby="nameHelp">
             </div>
            </div>
          </div> 
          <button type="submit" class="btn btn-primary btn-block">Simpan</button>
        </form>
      </div>
    </div>
  </div>
   <?php 
	}
   $this->load->view('./footer');
   ?>
