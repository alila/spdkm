<?php $this->load->view('./header');?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i>Tabel Data Mahasiswa Magang</div>
        <div class="card-body">
		<form method="post" action="<?php echo site_url('c_dok/simpan_mhs_magang')?>">
		<input type="text" name="id_magang" value="<?php echo $id?>">
          <div class="table-responsive">
	<button type="submit" class="btn btn-primary btn-block">Simpan</button>
					
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>No</th>
                  <th>NIM</th>
                  <th>Nama</th>
                  <th>Prodi</th>
                  <th>Tempat Lahir</th>
				  <th>Tanggal Lahir</th>
                  <th>Alamat</th>
				  <th>Pekerjaan Orangtua</th>
				  <th>Nama Orangtua</th>
				  <th>Action</th>
                </tr>
              </thead>
              <tbody>
				<?php 
				$no=1;
				foreach($Mhs as $m){?>
					<tr>
					  <td><?php echo $no++; ?></td>
					  <td><?php echo $m->nim ?></td>
					  <td><?php echo $m->nama ?></td>
					  <td><?php echo $m->prodi ?></td>
					  <td><?php echo $m->tempat_lahir ?></td>
					  <td><?php echo $m->tgl_lahir ?></td>
					  <td><?php echo $m->alamat ?></td>
					  <td><?php echo $m->pkrj_ortu ?></td>
					  <td><?php echo $m->nm_ortu ?></td>
					  
					  <td>
						  <input type="checkbox" name="Mhs[]" value="<?php echo $m->nim?>">
						</td>
						</tr>
                <?php }?>
              </tbody>
            </table>
			</form>
          </div>
        </div>
        <!-- <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>-->
      </div>
    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
<?php $this->load->view('./footer');?>
    