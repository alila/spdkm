<?php $this->load->view('./header');?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
       <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="<?php echo site_url('Mhs/home');?>">Dashboard</a>
        </li>
       </ol> 
	  
      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i> Data Tabel Surat Pengantar Penelitian Anda</div>
        <div class="card-body">
          <div class="table-responsive">
			
			<p><div class="btn btn-warning btn-block"><a href="<?php echo site_url('c_penelitian/tb_mhs_penelitian')?>">Tambah</a></div></p>
            
			<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>No</th>
                  <th>No Surat</th>
				  <th>Tempat Penelitian</th>
				  <th>Tanggal Mulai</th>
				  <th>Tanggal Selesai</th>
				  <th>Tertuju</th>
				  <th>Action</th>
                </tr>
              </thead>
              <tbody>
					<?php 
				$no=1;
				foreach($penelitian as $p){?>
					<tr>
					  <td><?php echo $no++; ?></td>
					  <td><?php echo $p->no_surat ?></td>
					  <td><?php echo $p->tempat_penelitian ?></td>
					  <td><?php echo $p->tgl_mulai ?></td>
					  <td><?php echo $p->tgl_selesai ?></td>
					  <td><?php echo $p->tertuju ?></td>
					  <td>
            
            <!-- "encode" the value before pass-->
            <?php $value = str_replace('/', '_', $p->no_surat); ?>

            <!-- "decode" the value after receive -->

            <a href="<?php echo base_url('c_penelitian/cetak_penelitian/'.$p->id_penelitian); ?>" class="btn-sm btn-success">Print</a>
						<a href="<?php echo site_url('c_penelitian/hapus_penelitian/'.$p->id_penelitian)?>" class="btn-sm btn-danger">Hapus</a>

					</td>
					</tr>
                <?php }?>
					</tr>
              </tbody>
            </table>
          </div>
        </div>
        <!-- <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>-->
      </div>
    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
<?php $this->load->view('./footer');?>
    