<?php $this->load->view('./header') ?>
<body class="bg-dark">
    <div class="container">
        <div class="card card-register mx-auto mt-5">
            <div class="card-header">Form Pengajuan<br>
                Surat Pengantar Penelitian</div>
            <div class="card-body">
                <form action="<?php echo site_url('c_penelitian/simpan_penelitian') ?>" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <div class="form-row">
                            
                            <div class="col-md-6">
                                <label for="tempat_penelitian">Tempat Penelitian</label>
                                <input name ="tempat_penelitian" class="form-control" id="exampleInputalamat" type="text" aria-describedby="nameHelp" required>
                            </div>
							<div class="col-md-6">
                                <label for="no_surat">No Surat</label>      
                                <input name ="no_surat" class="form-control" id="no_surat" type="text" value="<?php echo $no_surat?>" readonly="">
                           </div>
                            <div class="col-md-6">
                                <label for="tertuju">Tertuju</label>
                                <input name ="tertuju" class="form-control" id="tertuju" type="text" aria-describedby="tertuju" required>
                            </div>
                            <div class="col-md-6">
							
                                <div class="form-group input-append date form_datetime" data-date-format="yyyy-mm-dd">	<!---->
                                    <label for="exampleInputEmail1">Tanggal Mulai</label>
                                    <input class="form-control"  type="text" value="" name="tgl_mulai" readonly>
                                    <span class="add-on"><i class="icon-th"></i></span>
                                </div></div>
                            <div class="col-md-6">
                                <div class="form-group input-append date form_datetime"  data-date-format="yyyy-mm-dd">
                                    <label for="exampleInputEmail1">Tanggal Selesai</label>
                                    <input class="form-control"  type="text" value="" name="tgl_selesai" readonly>
                                    <span class="add-on"><i class="icon-th"></i></span>
                                </div></div>
                        </div>
                    </div> 

                    <button type="submit" class="btn btn-primary btn-block">Pengajuan</button>
                    <div class="btn btn-warning btn-block"><a href="<?php echo site_url('Mhs/home') ?>">Home</a></div>

                </form>   
            </div>
        </div>
    </div>

    <?php $this->load->view('./footer') ?>
