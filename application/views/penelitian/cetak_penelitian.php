<?php foreach ($penelitian as $p){?>
<html>
<head>
<link href="<?php echo base_url();?>/assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template-->
<link href="<?php echo base_url();?>/assets/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
</head>
<body>
<a title="Close" href="<?php echo site_url('c_penelitian/tb_penelitian');?>"> 
	<i class="fa fa-window-close" ></i> Close  
</a>

<table border="1" align="center" width="80%" height="900">
<tr><td valign="top">
<table  align="center" width="90%" border="0">
<tr>
	<p align="right"><?php echo $this->m_penelitian->tgl(date("Y/m/d"));?></p>
	<p align="left">
		No. 	: <?php echo $p->no_surat; ?><br>
		Hal 	: <b><u>Ijin melaksanakan penelitian</u></b>
	</p>
	<p align="left">Kepada Yth. :<br>
		<?php echo $p->tertuju; ?><br>
		<?php echo $p->tempat_penelitian; ?><br>
		Ditempat.
	</p>
	<br>
	<P>Dengan hormat,<br>
	Sehubungan dengan menyelesaikan salah satu tugas mahasiswa di Universitas Jenderal Achmad Yani, maka kami mengharapkan kesediaan Bapak/Ibu untuk mengijinkan/menerima mahasiswa kami:</p>
	<br>
	<table border=1 width="100%">
	<tr align='center'>
		<td>NO</td>
		<td>NIM</td>
		<td>NAMA</td>
		<td>PRODI</td>
	</tr>
		<?php 
		$no=1;
		foreach($penelitian as $pp){?>
	<tr align='center'>
		<td><?php echo $no++; ?></td>
		<td><?php echo $pp->nim; ?></td>
		<td><?php echo $pp->nama; ?></td>
		<td><?php echo $pp->prodi; ?></td>
	</tr>
	<?php }?>	
	</table>
	
	<p><br/>guna melaksanakan penelitian di <?php echo $pp->tempat_penelitian; ?> yang Bapak/Ibu pimpin.<br>
	Demikian surat permohonan ijin ini kami sampaikan, atas bantuan dan kerjasamanya diucapkan terimakasih.<br>
	</p>
	<p align='right'>
	A.n Dekan FTTI,<br>
	Ka. Prodi Teknik Informatika<br><br><br /><br />
	
	
	Andika Bayu Saputra, S.Kom.,M.Kom.
	</tp>


</tr>
</tr>
</table>
</table>


<a class="float" title="cetak halaman" onclick="cetak()"> 
	<i class="fa fa-print my-float" ></i>   
</a>
</html>
<?php }?>

<script>
function cetak(){
	print();
}
</script>
<style>
    @media print
    {    
        .float, .float *, .tombol
        {
            display: none !important;
        }
    }
    .float {
        position: fixed;
        width: 80px;
        height: 60px;
        bottom: 40px;
        right: 40px;
        background-color: Burlywood;
        color: #FFF;
        border-radius: 50px;
        text-align: center;
        box-shadow: 2px 2px 3px #999;
    }

    .my-float {
        margin-top: 22px;
    }
</style>
