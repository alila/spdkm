<?php $this->load->view('./header');?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
       <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="<?php echo site_url('Mhs/home');?>">Dashboard</a>
        </li>
       </ol> 
      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i> Data Tabel Surat Pengabdian Kemasyarakat Anda</div>
        <div class="card-body">
          <form method="post" action="<?php echo site_url('c_kkn/tb_mhs_kkn')?>">              
		  
		  <div class="table-responsive">
			<p><button type="submit" class="btn btn-primary btn-block">Tambah</button></p>
			<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
			  <thead>
                <tr>
                  <th>No</th>
                  <th>No Surat</th>
				  <th>Tempat kkn</th>
				  <th>Tanggal Mulai</th>
				  <th>Tanggal Selesai</th>
				  <th>Tertuju</th>
				  <th>Action</th>
                </tr>
              </thead>
              <tbody>
			  <?php 
				$no=1;
				foreach($kkn as $k){?>
					<tr>
					  <td><?php echo $no++; ?></td>
					  <td><?php echo $k->no_surat ?></td>
					  <td><?php echo $k->tempat_kkn ?></td>
					  <td><?php echo $k->tgl_mulai ?></td>
					  <td><?php echo $k->tgl_selesai ?></td>
					  <td><?php echo $k->tertuju ?></td>
					  <td>
						<a href="<?php echo site_url('c_kkn/cetak_kkn/'. $k->id_kkn)?>" class="btn-sm btn-success">Print</a>
						<a href="<?php echo site_url('c_kkn/hapus_kkn/'. $k->id_kkn)?>" class="btn-sm btn-danger">Hapus</a>
					</td>
					</tr>
                <?php }?>
              </tbody>
            </table>
          </div>
        </div>
        <!-- <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>-->
      </div>
    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
<?php $this->load->view('./footer');?>
    