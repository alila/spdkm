  <?php $this->load->view('./header')?>
<body class="bg-dark">
  <div class="container">
    <div class="card card-register mx-auto mt-5">
      <div class="card-header">Form Pengajuan<br>
	   Surat Pengantar Pengabdian Kemasyarakat</div>
      <div class="card-body">
        <form action="<?php echo site_url('c_kkn/simpan_kkn')?>" method="post" enctype="multipart/form-data">
          <div class="form-group">
            <div class="form-row">
			 
			  <div class="col-md-6">
                <label for="tempat_kkn">Tempat KKN</label>
                <input name ="tempat_kkn" class="form-control" id="exampleInputalamat" type="text" aria-describedby="nameHelp" required>
              </div>
			  
			  <input name ="no_surat" class="form-control" id="no_surat" type="hidden" value="<?php echo $no_surat?>" readonly="">
			  
			  <div class="col-md-6">
                <label for="tertuju">Tertuju</label>
                <input name ="tertuju" class="form-control" id="exampleInputProdi" type="text" aria-describedby="nameHelp" required>
              </div>
			  <div class="col-md-6">
				<div class="form-group input-append date form_datetime" data-date-format="yyyy-mm-dd">
                        <label for="exampleInputEmail1">Tanggal Mulai</label>
                        <input class="form-control"  type="text" value="" name="tgl_mulai" readonly>
                        <span class="add-on"><i class="icon-th"></i></span>
                    </div></div>
				<div class="col-md-6">
				<div class="form-group input-append date form_datetime" data-date-format="yyyy-mm-dd">
                        <label for="exampleInputEmail1">Tanggal Selesai</label>
                        <input class="form-control"  type="text" value="" name="tgl_selesai" readonly>
                        <span class="add-on"><i class="icon-th"></i></span>
                    </div></div>
				
            </div>
          </div> 
			<button type="submit" class="btn btn-primary btn-block">Pengajuan</button>
		</form>   
      </div>
    </div>
  </div>
  
  <?php $this->load->view('./footer')?>
