<footer class="sticky-footer">
      <div class="container">
        <div class="text-center">
          <small>Universitas Jenderal Achmad Yani Yogyakarta © 2018</small>
        </div>
      </div>
    </footer>
    
    <!-- Bootstrap core JavaScript-->
    <script src="<?php echo base_url();?>/assets/vendor/jquery/jquery.min.js"></script>
    <script src="<?php echo base_url();?>/assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- Core plugin JavaScript-->
    <script src="<?php echo base_url();?>/assets/vendor/jquery-easing/jquery.easing.min.js"></script>
    <!-- Custom scripts for all pages-->
    <script src="<?php echo base_url();?>/assets/js/sb-admin.min.js"></script>
	<script src="<?php echo base_url();?>/assets/vendor/datatables/jquery.dataTables.js"></script>
    <script src="<?php echo base_url();?>/assets/vendor/datatables/dataTables.bootstrap4.js"></script>
    <!-- Custom scripts for all pages-->
    <script src="<?php echo base_url();?>/assets/js/sb-admin-datatables.min.js"></script>
	
	
	
	<script type="text/javascript" src="<?php echo base_url('assets/date_picker_bootstrap/js/bootstrap-datetimepicker.js') ?>" charset="UTF-8"></script>
	<script type="text/javascript" src="<?php echo base_url('assets/date_picker_bootstrap/js/locales/bootstrap-datetimepicker.id.js') ?>" charset="UTF-8"></script>
	
	<script type="text/javascript">
    $('.form_datetime').datetimepicker({
        language: 'id',
        weekStart: 1,
        todayBtn: 1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 2,
        minView: 2,
        forceParse: 0
    });
</script>
	
  </div>
</body>

</html>
