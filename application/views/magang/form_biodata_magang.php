<?php $this->load->view('./header')?>
<body class="bg-dark">
  <div class="container">
    <div class="card card-register mx-auto mt-5">
      <div class="card-header">Form Biodata Peserta Magang</div>
      <div class="card-body">
        <form action="<?php echo site_url('c_magang/ajukan_biodata')?>" method="post" enctype="multipart/form-data">
          <div class="form-group">
            <div class="form-row">
              <div class="col-md-6">
                <label for="exampleInputName">Nama Instansi</label>
                <input name ="nama_instansi" class="form-control" id="nama_instansi" type="text" aria-describedby="nameHelp" required>
              </div>
			  <div class="col-md-6">
                <label for="alamat_instansi">Alamat Instansi</label>
                <input name ="alamat_instansi" class="form-control" id="alamat_instansi" type="text" aria-describedby="nameHelp" required>
              </div>
			  <div class="col-md-6">
                <label for="exampleInputName">No. Telp</label>
                <input name ="no_telp" class="form-control" id="no_telp" type="text" aria-describedby="nameHelp" required>
              </div>
              <div class="col-md-6">
                <label for="tertuju">Tertuju Surat</label>
                <input name ="tertuju" class="form-control" id="tertuju" type="text" aria-describedby="nameHelp" required>
              </div>
			  <div class="col-md-6">
                <label for="kota">Kota</label>
				<select name= 'kota' class="form-control" id="kota">
					<?php $kota= $this->M_dok->data_kota();
					foreach ($kota as $kt){
					?>
					<option value="<?php echo $kt->nama?>"><?php echo $kt->nama?></option>
					<?php } ?>
				 </select>
              </div>
		  <div class="col-md-6">
			 <div class="form-group input-append date form_datetime" data-date-format="yyyy-mm-dd">
                        <label for="exampleInputEmail1">Tanggal Mulai</label>
                        <input class="form-control"  type="text" value="" name="tgl_mulai" readonly>
                        <span class="add-on"><i class="icon-th"></i></span>
                    </div> </div>
			<div class="col-md-6">		
			<div class="form-group input-append date form_datetime" data-date-format="yyyy-mm-dd">
                        <label for="exampleInputEmail1">Tanggal Selesai</label>
                        <input class="form-control"  type="text" value="" name="tgl_selesai" readonly>
                        <span class="add-on"><i class="icon-th"></i></span>
                    </div> </div>
          <button type="submit" class="btn btn-primary btn-block">Simpan</button>
        </form>
      </div>
    </div>
  </div>
  </div>
  </div>
  </body>

<?php $this->load->view('./footer')?>
