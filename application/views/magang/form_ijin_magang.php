 <?php $this->load->view('./header')?>

<body class="bg-dark">
  <div class="container">
    <div class="card card-register mx-auto mt-5">
      <div class="card-header">Form Ijin Magang</div>
      <div class="card-body">
        <form action="<?php echo site_url('c_magang/simpan_form_ijin_magang/'.$datas->id)?>" method="post" enctype="multipart/form-data">
          <div class="form-group">
            <div class="form-row">
              <div class="col-md-6">
                <label for="exampleInputName">Total SKS</label>
                <input name ="total_sks" class="form-control" id="total_sks" type="text" aria-describedby="nameHelp" required>
              </div>


              <div class="col-md-6">
                <label for="ipk">IPK</label>
                <input name ="ipk" class="form-control" id="ipk" type="text" aria-describedby="nameHelp" required>
              </div>

              <div class="col-md-6">
                <label for="nilai_d">Total Nilai D</label>
                <input name ="nilai_d" class="form-control" id="nilai_d" type="text" aria-describedby="emailHelp" required>
              </div>

              <div class="col-md-6">
                <label for="nilai_e">Total Nilai E</label>
                <input name ="nilai_e" class="form-control" id="nilai_e" type="text" aria-describedby="emailHelp" required>
              </div>

              <div class="col-md-6">
                <label for="dosbing">Nama Dosbing</label>
                <select name= 'dosbing' class="form-control" id="dosbing">
                  <?php 
                    $dosbing= $this->m_magang->data_dosbing();
                    foreach ($dosbing as $do){
                      ?>
                  <option value="<?php echo $do->nama?>"><?php echo $do->nama?></option>
                  <?php } ?>
                </select>
              </div>
		   </div>
		   </div>
          <button type="submit" class="btn btn-primary btn-block">Simpan</button>
       
	   </form>
      </div>
    </div>
  </div>
   </div>
</body>
<?php $this->load->view('./footer')?>
