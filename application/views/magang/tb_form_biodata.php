<?php $this->load->view('./header') ?>

<body class="fixed-nav sticky-footer bg-dark" id="page-top">
    <!-- Navigation-->
    <div class="content-wrapper">
        <div class="container-fluid">
            <!-- Breadcrumbs-->
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="#">Dashboard</a>
                </li>
                <li class="breadcrumb-item active">Tabel Data Magang</li>
            </ol>
            <!-- Example DataTables Card-->
            <div class="card mb-3">
                <div class="card-header">
                    <i class="fa fa-table"></i>Tabel Data Magang Anda</div>
                <div class="card-body">
					<form method="post" action="<?php echo site_url('c_magang/form_ijin_magang')?>">           
 
                    <div class="table-responsive">
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead align='center'>
                                <tr>
                                    <th>No.</th>
                                    <th>NIM</th>
                                    <th>Nama</th>
                                    <th>No. Surat</th>
                                    <th>Nama Instansi</th>
                                    <th>Tanggal Mulai</th>
									<th>Tanggal Selesai</th>
                                    <th>Dosbing</th>
                                    <th>Action</th>
                                </tr>
                            </thead> 
                            <tbody align='center'>
							<?php 
								$no=1;
									foreach($magang as $m){?>
                                <tr>
                                    <td><?php echo $no++; ?></td>
									<td><?php echo $m->nim ?></td>
									<td><?php echo $m->nama ?></td>
									<td><?php echo $m->no_surat ?></td>
									<td><?php echo $m->nama_instansi ?></td>
									<td><?php echo $m->tgl_mulai ?></td>
									<td><?php echo $m->tgl_mulai ?></td>
									<td><?php echo $m->dosbing ?></td>
                                    <td>
										<p><a href="<?php echo base_url('c_magang/form_ijin_magang/'.$m->id); ?>" class="btn-sm btn-warning">Formulir</a></p>
                                        <p><a href="<?php echo site_url('c_magang/cetak_form_ijin_magang/'. $this->session->userdata('id')); ?>" class="btn-sm btn-success">Print Formulir</a></p>
                                        <p><a href="<?php echo site_url('c_magang/cetak_biodata_magang/'.$m->id_magang); ?>" class="btn-sm btn-info">Print Biodata</a></p>
                                    </td>
                                </tr>
								 <?php }?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
            </div>
        </div>
        <!-- /.container-fluid-->
        <!-- /.content-wrapper-->
        <footer class="sticky-footer">
            <div class="container">
                <div class="text-center">
                    <small>Copyright © Your Website 2018</small>
                </div>
            </div>
        </footer>
        <!-- Scroll to Top Button-->
        <a class="scroll-to-top rounded" href="#page-top">
            <i class="fa fa-angle-up"></i>
        </a>
        <!-- Logout Modal-->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                        <a class="btn btn-primary" href="login.html">Logout</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

<?php $this->load->view('./footer') ?>
