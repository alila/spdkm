<?php foreach ($magang as $m){?>
<html>
<head>
	<link href="<?php echo base_url();?>/assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<!-- Custom fonts for this template-->
	<link href="<?php echo base_url();?>/assets/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
</head>
<body>
	<a title="Close" href="<?php echo site_url('c_magang/tb_biodata');?>"> 
	<i class="fa fa-window-close" ></i> Close </a>
<table border="1" align="center" width="80%" height="900">
<tr><td valign="top">
<table  align="center" width="90%" border="0">
<tr>
	<td align="center" valign="top" colspan="3">
	   <h4><u><b>BIODATA PESERTA MAGANG</b></u></h4>
	   <b>No : <?php echo $m->kode_form; ?></b>
	</td><br><br>
</tr>
<tr>
	<td align="left">
		<b>Kepada Yth. <br>
		<?php echo $m->tertuju; ?> <?php echo $m->nama_instansi; ?><br>
		Di_ <br>
			Yogyakarta</b>
	</td>
</tr>
	
<tr>
	<td colspan="2">
	<table width="100%">
	<br>
	<td colspan="3">Dengan ini saya :<br><br>
	
	<table border=1 width="100%">
	<tr align='center'>
		<td>NO</td>
		<td>NIM</td>
		<td>NAMA</td>
	</tr>
	<?php 
		$no=1;
			foreach($magang1 as $sm){?>
	<tr align='center'>
		<td><?php echo $no++; ?></td>
		<td><?php echo $sm->nama; ?></td>
		<td><?php echo $sm->nim; ?></td>
	</tr>
	<?php }?>	
	</table>
	
	<table>
	
	<tr><br>
	<p>mengajukan permohonan Surat Pengantar Magang Kerja pada :</p>
		<p>Nama Instansi 	: <?php echo $m->nama_instansi; ?></p>
		<p>Alamat Instansi 	: <?php echo $m->alamat_instansi; ?></p>
		<p>No. Telp. 		: <?php echo $m->no_telp; ?></p>
		<p>Tertuju Surat 	: <?php echo $m->tertuju; ?></p>
		<p>Waktu Magang 	: <?php echo $m->tgl_mulai; ?> - <?php echo $m->tgl_selesai; ?></p>
	<p>Demikian surat permohonan ini saya sampaikan, atas perhatian dan perkenannya diucapkan terimakasih.</p>
	</tr>
	<p align='right'>Yogyakarta, <?php echo $this->m_magang->tgl(date("Y/m/d"));?></p>
	
	</table><br>
<table align='left'>
<tr>
	<td colspan="2"></td><td width="300">
	Menyetujui,<br>
	Ka. Prodi Teknik Informatika<br><br><br /><br />
	
	
	Andika Bayu Saputra, S.Kom., M.Kom.
	</td>
</tr>
</table>

<table align='right'>
<?php 
		$no=1;
			foreach($mhs as $m){?>
<tr>
	<td colspan="2" ></td><td width="250">
	Mahasiswa yang mengajukan<br>
	<br><br><br /><br />
	
	
	<?php echo $m->nama; ?>
	</td>
</tr>
<?php }?> 
</table>
</tr>

</tr>
</table>
</td></tr>
</table>
<a class="float" title="cetak halaman" onclick="cetak()"> 
	<i class="fa fa-print my-float" ></i>   
</a>
</html>
<?php }?>  

<script>
function cetak(){
	print();
}
</script>
<style>
    @media print
    {    
        .float, .float *, .tombol
        {
            display: none !important;
        }
    }
    .float {
        position: fixed;
        width: 80px;
        height: 60px;
        bottom: 40px;
        right: 40px;
        background-color: Burlywood;
        color: #FFF;
        border-radius: 50px;
        text-align: center;
        box-shadow: 2px 2px 3px #999;
    }

    .my-float {
        margin-top: 22px;
    }
</style>
