<?php $this->load->view('./header');?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i>Tabel Data Mahasiswa Magang</div>
        <div class="card-body">
		
		<form method="post" action="<?php echo site_url('c_magang/simpan_mhs_magang')?>">           
		<input type="hidden" name="id_magang" value="<?php echo $id_magang; ?>"> 
          
		  <div class="table-responsive">
			<p><button type="submit" class="btn btn-primary btn-block">Simpan</button></p>
					
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>No</th>
                  <th>NIM</th>
                  <th>Nama</th>
                  <th>Prodi</th>
                  <th>Tempat Lahir</th>
				  <th>Tanggal Lahir</th>
				  <th>Nama Orangtua</th>
				  <th>Pekerjaan Orangtua</th>
                  <th>Alamat</th>
				  <th>Action</th>
                </tr>
              </thead>
              <tbody>
			  <?php 
				$no=1;
				foreach($mhs as $m){?>
					<tr>
						<td><?php echo $no++; ?></td>
						<td><?php echo $m->nim ?></td>
						<td><?php echo $m->nama ?></td>
						<td><?php echo $m->prodi ?></td>
						<td><?php echo $m->tempat_lahir ?></td>
						<td><?php echo $m->tgl_lahir ?></td>
						<td><?php echo $m->nama_ortu ?></td>
						<td><?php echo $m->pekerjaan_ortu ?></td>
						<td><?php echo $m->alamat ?></td>
					  <td>
						  <input type="checkbox" name="Mhs[]" value="<?php echo $m->nim; ?>" <?php if ($m->nim == $this->session->userdata('id')){?> 
						  checked
						  <?php }?> >
						</td>
					</tr>
                <?php }?>
              </tbody>
            </table>
			</form>
          </div>
        </div>
        <!-- <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>-->
      </div>
    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
<?php $this->load->view('./footer');?>
    