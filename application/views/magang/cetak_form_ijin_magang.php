<?php foreach ($magang as $m){?>
<html>
<head>
	<link href="<?php echo base_url();?>/assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<!-- Custom fonts for this template-->
	<link href="<?php echo base_url();?>/assets/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
</head>
<body>
	<a title="Close" href="<?php echo site_url('c_magang/tb_biodata');?>"> 
	<i class="fa fa-window-close" ></i> Close </a>
	
<table border="1" align="center" width="80%" height="900">
<tr><td valign="top">

<table  align="center" width="90%" border="0">
<tr>
	<td align="left" valign="top" colspan="3">
	   <h4>Formulir Ijin Magang</h4>
	</td><br>
</tr>
<tr>
	<td align="left">
	Dosen pembimbing akademik untuk mahasiswa berikut : <br><br>
	Nama : <?php echo $m->nama; ?><br>
	NIM : <?php echo $m->nim; ?><br>
	</td>
</tr>
		
			
		
<tr>
	<td colspan="2">
	<table width="100%">
	<br>
	<td colspan="3">Menyatakan mengijinkan mahasiswa tersebut melaksanakan magang kerja berdasarkan pertimbangan persyaratan akademik dan kesiapan mahasiswa.<br><br>
	</table>
	<table align='left'>
<tr>
	<td colspan="1"></td>
	Yogyakarta, <?php echo $this->m_magang->tgl(date("Y/m/d"));?><br>
	<br><br><br /><br />
	
	
	(<?php echo $m->dosbing; ?>)
	</td><br>
</tr><br>
<tr>
	<td>
	Catatan:
	</td>
</tr>
</table><br><br>
<table border=1 width='50%' border=5 cellpadding=15 cellspacing=15>


	<tr>
		<td colspan="3"><br/>
		Total SKS diambil: <?php echo $m->total_sks; ?> SKS<br>
		IPK: <?php echo $m->ipk; ?><br>
		Total nilai: D : <?php echo $m->nilai_d; ?><br>
					 E : <?php echo $m->nilai_e; ?><br>
		</td>
	</tr>

</table><br><br>

<table align='left'>
<tr>
	<td colspan="2" ></td><td width="100%">
	Persyaratan akademis Peserta Magang<br>
	Mahasiswa dapat melaksanakan Magang apabila:<br>
	<ol type='1'>
		<li>Telah menempuh semester 6 dan lulus atau menunggu nilai keluar minimal 125 SKS.</li>
		<li>Nilai Indeks Prestasi Kumulatif (IPK) tidak kurang 2.00 atau dengan pertimbangan tertentu berdasarkan kebijakan dosen pembimbing Akademik.</li>
		<li>Masih terdaftar di Universitas Jend. A. Yani Yogyakarta secara resmi dan berstatus aktif dalam tahun akademik berjalan.</li>
		<li>Tidak sedang menjalani sanksi akibat pelanggaran akademis tertentu.</li>
	</ul>
	</td>
</tr>
</table>
</tr>
<tr>
	
</tr>
</tr>
</table>

</td></tr>
</table>

<a class="float" title="cetak halaman" onclick="cetak()"> 
	<i class="fa fa-print my-float" ></i>   
</a>

</html>
<?php }?> 

<script>
function cetak(){
	print();
}
</script>
<style>
    @media print
    {    
        .float, .float *, .tombol
        {
            display: none !important;
        }
    }
    .float {
        position: fixed;
        width: 80px;
        height: 60px;
        bottom: 40px;
        right: 40px;
        background-color: Burlywood;
        color: #FFF;
        border-radius: 50px;
        text-align: center;
        box-shadow: 2px 2px 3px #999;
    }

    .my-float {
        margin-top: 22px;
    }
</style>
