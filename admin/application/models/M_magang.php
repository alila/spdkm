<?php

class M_magang extends CI_Model {

    function proses_input_db($data) {
        $this->db->insert('tb_magang', $data);
    }

    function magang() {
        $query = $this->db->query("SELECT * FROM tb_magang");
        return $query->result();
    }
    function cetak_magang($id) {
        $query = $this->db->query("SELECT * FROM tb_magang where id_magang='$id'");
        return $query->result();
    }
    function cetak_mhs_magang($id) {
        $query = $this->db->query("SELECT * FROM tb_magang a, tb_mhs_magang b, tb_mahasiswa c where a.id_magang= b.id_magang and b.nim=c.nim and a.id_magang='$id'");
        return $query->result_array();
    }
    function edit_magang($id) {
        $query = $this->db->query("SELECT * FROM tb_magang where id_magang='$id'");
        return $query->result();
    }
	function proses_edit_db($data,$id){
		$this->db->where('id_magang',$id);
		$this->db->update('tb_magang',$data);
	}
	function hapus_magang($id){
		$this->db->where('id_magang', $id);
		$this->db->delete('tb_magang');
	}
	function smt(){
		$now = date('n');
		$th_now = date('Y');
		$th_next = date ('Y', strtotime('+1 years'));
		$th_before = date ('Y', strtotime('-1 years'));
		switch ($now){
			case 9;
			case 10;
			case 11;
			case 12;
			case 1;
			case 2;
		$bulan = "Semester Ganjil Tahun Akademik $th_now/$th_next";
		break;
			case 3;
			case 4;
			case 5;
			case 6;
			case 7;
			case 8;
		$bulan = "Semester Genap Tahun Akademik $th_before/$th_now";
		break;
		} return $bulan;
	}
	function tgl($tgl){
		$hari = substr($tgl, 8, 2);
        $tahun = substr($tgl, 0, 4);
        $nama_bulan = $this->bulan($tgl);
        $tgl_oke = $hari . ' ' . $nama_bulan . ' ' . $tahun;
        return $tgl_oke;
	}
	function bulan($tgl){
		$bulan = substr($tgl, 5, 2);
        Switch ($bulan) {
            case 1 : $bulan = "Januari";
                Break;
            case 2 : $bulan = "Februari";
                Break;
            case 3 : $bulan = "Maret";
                Break;
            case 4 : $bulan = "April";
                Break;
            case 5 : $bulan = "Mei";
                Break;
            case 6 : $bulan = "Juni";
                Break;
            case 7 : $bulan = "Juli";
                Break;
            case 8 : $bulan = "Agustus";
                Break;
            case 9 : $bulan = "September";
                Break;
            case 10 : $bulan = "Oktober";
                Break;
            case 11 : $bulan = "November";
                Break;
            case 12 : $bulan = "Desember";
                Break;
        }
        return $bulan;
	}

}

?>