<?php
class Adm_m extends CI_Model{
	
	//Admin Start
	function proses_input_db($data){
		$this->db->insert('tb_admin', $data);
	}
	function input_adm(){
		$query = $this->db->query("SELECT * FROM tb_admin");
		return $query->result();
	}
	function list_adm($nip){
		$query = $this->db->query("SELECT * FROM tb_admin where nip='$nip' ");
		return $query->result();
	}
	function hapus_adm($nip){
		$this->db->where('nip', $nip);
		$this->db->delete('tb_admin');
	}
	function edit_adm($nip){
		$query = $this->db->query("SELECT * FROM tb_admin WHERE nip='$nip'");
		return $query->result();
	}
	function proses_edit_db($data, $nip){
		$this->db->where('nip', $nip);
		$this->db->update('tb_admin', $data);
	}
	// Admin End
	
	// Mahasiswa Start
	function proses_input_mhs_db($data){
		$this->db->insert('tb_mahasiswa', $data);
	}
	function list_mhs(){
		$query = $this->db->query("SELECT * FROM tb_mahasiswa");
		return $query->result();
	}
	function hapus_mhs($nim){
		$this->db->where('nim', $nim);
		$this->db->delete('tb_mahasiswa');
	}
	function edit_mhs($nim){
		$query = $this->db->query("SELECT * FROM tb_mahasiswa WHERE nim='$nim'");
		return $query->result();
	}
	function proses_edit_mhs_db($data, $nim){
		$this->db->where('nim', $nim);
		$this->db->update('tb_mahasiswa', $data);
	}
	// Mahasiswa End
	
}
?>