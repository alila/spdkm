<?php $this->load->view('./header');
foreach($c_dokumen as $sa){
?>
<body class="bg-dark">
  <div class="container">
    <div class="card card-register mx-auto mt-5">
      <div class="card-header">Form Edit Data Surat Pengantar Penelitian</div>
      <div class="card-body">
        <form action="<?php echo site_url('c_dokumen/proses_edit_penelitian/'. $sa->id_penelitian)?>" method="post">
          <div class="form-group">
            <div class="form-row">
			        <div class="col-md-6">
                <label for="id">ID</label>
                <input readonly value="<?php echo $sa->id_penelitian ?>" name ="id_penelitian" class="form-control" id="id" type="text" aria-describedby="nameHelp">
              </div>

              <div class="col-md-6">
                <label for="tempat_kkn">No Surat</label>
                <input readonly value="<?php echo $sa->no_surat ?>" name ="no_surat" class="form-control" id="no_surat" type="text" aria-describedby="nameHelp" readonly="">
              </div>

              <div class="col-md-6">
                <label for="tempat_kkn">NIM</label>
                <input value="<?php echo $sa->nim ?>" name ="nim" class="form-control" id="nim" type="text" aria-describedby="nameHelp">
              </div>

              <div class="col-md-6">
                <label for="tempat_kkn">Tempat Penelitian</label>
                <input value="<?php echo $sa->tempat_penelitian ?>" name ="tempat_penelitian" class="form-control" id="tempat_penelitian" type="text" aria-describedby="nameHelp">
              </div>

			        <div class="col-md-6">
                <label for="tgl_mulai">Tanggal Mulai</label>
                <input value="<?php echo $sa->tgl_mulai ?>" style="text-color:black" name ="tgl_mulai" class="form-control" id="tgl_mulai" type="date" aria-describedby="nameHelp">
              </div>
              
			        <div class="col-md-6">
                <label for="tgl_selesai">Tanggal Selesai</label>
                <input value="<?php echo $sa->tgl_selesai ?>" name ="tgl_selesai" class="form-control" id="tgl_selesai" type="date" aria-describedby="nameHelp">
              </div>

              <div class="col-md-6">
                <label for="tempat_kkn">Tempat Penelitian</label>
                <input value="<?php echo $sa->tempat_penelitian ?>" name ="tempat_penelitian" class="form-control" id="tempat_penelitian" type="text" aria-describedby="nameHelp">
              </div>
              
			        <div class="col-md-6">
                <label for="tertuju">Tertuju</label>
                <input value="<?php echo $sa->tertuju ?>" name ="tertuju" class="form-control" id="tertuju" type="text" aria-describedby="nameHelp">
              </div>
            </div>
          </div> 
          <button type="submit" class="btn btn-primary btn-block">Simpan</button>
        </form>
      </div>
    </div>
  </div>
   <?php 
	}
   $this->load->view('./footer');
   ?>
