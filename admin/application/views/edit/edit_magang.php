<?php $this->load->view('./header');
foreach($c_dokumen as $sa){
?>
<body class="bg-dark">
  <div class="container">
    <div class="card card-register mx-auto mt-5">
      <div class="card-header">Form Edit Data Surat Keterangan Magang Kuliah</div>
      <div class="card-body">
        <form action="<?php echo site_url('c_dokumen/proses_edit_magang/'. $sa->id_magang)?>" method="post">
          <div class="form-group">
            <div class="form-row">
			        <div class="col-md-6">
                <label for="id">ID</label>
                <input readonly value="<?php echo $sa->id_magang ?>" name ="id_magang" class="form-control" id="id" type="text" aria-describedby="nameHelp">
              </div>

              <div class="col-md-6">
                <label for="no_surat">No Surat</label>
                <input readonly value="<?php echo $sa->no_surat ?>" name ="no_surat" class="form-control" id="no_surat" type="text" aria-describedby="nameHelp" readonly="">
              </div>

              <div class="col-md-6">
                <label for="nama_instansi">Nama Instansi</label>
                <input value="<?php echo $sa->nama_instansi ?>" name ="nama_instansi" class="form-control" id="nama" type="text" aria-describedby="nameHelp">
              </div>

			        <div class="col-md-6">
                <label for="tgl_mulai">Tanggal Mulai</label>
                <input value="<?php echo $sa->tgl_mulai ?>" style="text-color:black" name ="tgl_mulai" class="form-control" id="tgl_mulai" type="date" aria-describedby="nameHelp">
              </div>
              
			        <div class="col-md-6">
                <label for="tgl_selesai">Tanggal Selesai</label>
                <input value="<?php echo $sa->tgl_selesai ?>" name ="tgl_selesai" class="form-control" id="tgl_selesai" type="date" aria-describedby="nameHelp">
              </div>
              
			        <div class="col-md-6">
                <label for="alamat">Alamat</label>
                <input value="<?php echo $sa->alamat_instansi ?>" name ="alamat" class="form-control" id="alamat" type="text" aria-describedby="nameHelp">
              </div>
              
			        <div class="col-md-6">
                <label for="tertuju">Tertuju</label>
                <input value="<?php echo $sa->tertuju ?>" name ="tertuju" class="form-control" id="tertuju" type="text" aria-describedby="nameHelp">
              </div>

			        <div class="col-md-6">
                <label for="no_hp">No Hp</label>
                <input value="<?php echo $sa->no_telp ?>" name ="no_hp" class="form-control" id="no_hp" type="text" aria-describedby="nameHelp">
              </div>
            </div>
          </div> 
          <button type="submit" class="btn btn-primary btn-block">Simpan</button>
        </form>
      </div>
    </div>
  </div>
   <?php 
	}
   $this->load->view('./footer');
   ?>
