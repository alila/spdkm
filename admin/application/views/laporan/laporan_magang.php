<html>
<head>
<link href="<?php echo base_url();?>/assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template-->
<link href="<?php echo base_url();?>/assets/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
</head>

<body>

<a title="Close" href="<?php echo site_url('Adm/laporan_magang');?>"> 
	<i class="fa fa-window-close" ></i>Close  
</a>

<table border="1" align="center" width="80%" height="900">
<tr><td valign="top">

<table  align="center" width="90%" border="0">
<tr>
	<td align="center" valign="top" colspan="3">
	<img src="<?php echo base_url('assets/foto/logo.jpg');?>" width="120px" height="120px"  style ="float:left; margin-bottom: 10px;"/>
	   <h4><u>UNIVERSITAS JENDERAL ACHMAD YANI YOGYAKARTA</u></h4>
	   <h5>Laporan Rekap Surat Pengantar Magang Kerja <?php echo $this->M_magang->smt();?></h5>
	</td>
		<br>
</tr>

			
<tr>
	<td colspan="2">
	<table width="100%">

	
	<tr></tr>
	<br>
	<td colspan="3">
	<table border=1 width="100%">
	<tr align='center'>
		<td>No</td>
		<td>NIM</td>
		<td>Nama</td>
		<td>Prodi</td>
		<td>Nama Instansi</td>
		<td>Tanggal Mulai</td>
		<td>Tanggal Selesai</td>
	</tr>
	<?php 
		$no=1;
		foreach($magang as $m){
	?>	
	<tr align='center'>
		<td><?php echo $no++; ?></td>
		<td><?php echo $m->nim; ?></td>
		<td><?php echo $m->nama; ?></td>
		<td><?php echo $m->prodi; ?></td>
		<td><?php echo $m->nama_instansi; ?></td>
		<td><?php echo $this->M_magang->tgl($m->tgl_mulai); ?></td>
		<td><?php echo $this->M_magang->tgl($m->tgl_selesai); ?></td>
	</tr>
		<?php }?>
	</table>
	<tr><td colspan="3">

</tr>
</table>

</td></tr>
</table>


<a class="float" title="cetak halaman" onclick="cetak()"> 
	<i class="fa fa-print my-float" ></i>   
</a>
</html>


<script>
function cetak(){
	print();
}
</script>
<style>
    @media print
    {    
        .float, .float *, .tombol
        {
            display: none !important;
        }
    }
    .float {
        position: fixed;
        width: 80px;
        height: 60px;
        bottom: 40px;
        right: 40px;
        background-color: Burlywood;
        color: #FFF;
        border-radius: 50px;
        text-align: center;
        box-shadow: 2px 2px 3px #999;
    }

    .my-float {
        margin-top: 22px;
    }
</style>
