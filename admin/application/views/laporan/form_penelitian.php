<?php $this->load->view('./header')?>
 
<body class="bg-dark">
  <div class="container">
    <div class="card card-register mx-auto mt-5">
      <div class="card-header">Form Laporan <br>
	  Surat Pengantar Penelitian</div>
      <div class="card-body">
        <form action="<?php echo site_url('Adm/tampil_laporan_penelitian')?>" method="post" enctype="multipart/form-data">
          <div class="form-group">
            <div class="form-row">
			  <div class="col-md-6">
			 <div class="form-group input-append date form_datetime" data-date-format="yyyy-mm-dd">
                        <label for="exampleInputEmail1">Tanggal Mulai</label>
                        <input class="form-control"  type="text" value="" name="tgl_mulai" readonly>
                        <span class="add-on"><i class="icon-th"></i></span>
                    </div> </div>
			<div class="col-md-6">		
			<div class="form-group input-append date form_datetime" data-date-format="yyyy-mm-dd">
                        <label for="exampleInputEmail1">Tanggal Selesai</label>
                        <input class="form-control"  type="text" value="" name="tgl_selesai" readonly>
                        <span class="add-on"><i class="icon-th"></i></span>
                    </div> </div>	 
            </div>
          </div> 
			<button type="submit" class="btn btn-primary btn-block">Tampil</button>
        </form>   
      </div>
    </div>
  </div>
  
  <?php $this->load->view('./footer')?>
  
	<script type="text/javascript">
    $('.form_datetime').datetimepicker({
		format:"yyyy-mm-dd",
        language: 'id',
        weekStart: 1,
        todayBtn: 1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 2,
        minView: 2,
        forceParse: 0
    });
</script>
