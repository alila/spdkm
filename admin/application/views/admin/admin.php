<?php $this->load->view('./header');?>
<body class="bg-dark">
  <div class="container">
    <div class="card card-register mx-auto mt-5">
      <div class="card-header">Form Data Admin</div>
      <div class="card-body">
        <form action="<?php echo site_url('Adm/proses_input')?>" method="post" enctype="multipart/form-data">
          <div class="form-group">
            <div class="form-row">
              <div class="col-md-6">
                <label for="exampleInputNIP">NIP</label>
                <input name ="nip" class="form-control" id="exampleInputNIP" type="text" aria-describedby="nameHelp">
              </div>
			  <div class="col-md-6">
                <label for="exampleInputnama">Nama</label>
                <input name ="nama" class="form-control" id="exampleInputnama" type="text" aria-describedby="nameHelp">
              </div>
              <div class="col-md-6">
                <label for="exampleInputpass">Password</label>
                <input name ="password" class="form-control" id="exampleInputpass" type="password" aria-describedby="nameHelp">
              </div>
			  <div class="col-md-6">
                <label for="exampleInputid_penelitian">Foto</label>
                <input name ="file-foto" class="form-control" type="file" aria-describedby="nameHelp" required>
              </div>
            </div>
          </div> 
          <button type="submit" class="btn btn-primary btn-block">Simpan</button>
        </form>
        
      </div>
    </div>
  </div>
   <?php $this->load->view('./footer');?>
