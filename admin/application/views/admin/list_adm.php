<?php $this->load->view('./header');?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="index.html">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Data Admin</li>
      </ol>
	  <p><a href="<?php echo site_url('Adm/view_input_adm')?>" class="btn btn-primary">Tambah Data Admin</a></p>
      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i> Data Table Admin</div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
					<th>No</th>
					<th>NIP</th>
					<th>Nama</th>
					<th>Password</th>
					<th>Foto</th>
					<th>Action</th>
                </tr>
              </thead>
              <tbody>
				<?php 
				$no=1;
				foreach($admin as $a){?>
					<tr>
						<td><?php echo $no++; ?></td>
						<td><?php echo $a->nip ?></td>
						<td><?php echo $a->nama ?></td>
						<td><?php echo $a->password ?></td>	
						<td><img src="<?php echo base_url('assets/foto/'.$a->foto_adm); ?>" class='img-responsive' width='69' height='69'/></td>
						<td>
						  <a href="<?php echo site_url('Adm/hapus_adm/'. $a->nip)?>" class="btn btn-danger" onclick="return confirm('Apakah Anda Yakin Akan Menghapus Data Ini ?') ">Hapus</a>
						  <a href="<?php echo site_url('Adm/edit_adm/'. $a->nip)?>" class="btn btn-warning">Edit</a>
						</td>
					</tr>
                <?php }?>
              </tbody>
            </table>
          </div>
        </div>
        <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
      </div>
    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
<?php $this->load->view('./footer');?>
    