<?php $this->load->view('./header');?>
<?php 
	$no=1;
	foreach($admin as $a){
?>
<body class="bg-dark">
  <div class="container">
    <div class="card card-register mx-auto mt-5">
      <div class="card-header">Form Data Admin</div>
      <div class="card-body">
        <form action="<?php echo site_url('Adm/proses_edit')?>" method="post" enctype="multipart/form-data">
          <div class="form-group">
            <div class="form-row">
              <div class="col-md-6">
                <label for="exampleInputNIM">NIP</label>
                <input value="<?php echo $a->nip ?>" name ="nip" class="form-control" id="nip" type="text" aria-describedby="nameHelp" readonly="">
              </div>
              <div class="col-md-6">
                <label for="exampleInputName">Nama</label>
                <input value="<?php echo $a->nama ?>" name ="nama" class="form-control" id="exampleInputNama" type="text" aria-describedby="nameHelp">
              </div>
			  <div class="col-md-6">
                <label for="exampleInputProdi">Password</label>
                <input value="<?php echo $a->password ?>" name ="password" class="form-control" id="exampleInput" type="password" aria-describedby="nameHelp">
              </div>
			<div class="col-md-6">
                <label for="exampleInputpek_ortu">Foto</label>
                <input value="" name ="file_foto" class="form-control" id="exampleInputpek_ortu" type="file" aria-describedby="nameHelp" required>
             </div>
            </div>
          </div> 
          <button type="submit" class="btn btn-primary btn-block">Simpan</button>
        </form>
      </div>
    </div>
  </div>
   <?php 
	} ?>
   <?php $this->load->view('./footer');?>
