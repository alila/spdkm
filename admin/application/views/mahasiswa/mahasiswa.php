<?php $this->load->view('./header');?>
<body class="bg-dark">
  <div class="container">
    <div class="card card-register mx-auto mt-5">
      <div class="card-header">Form Tambah Data Mahasiswa</div>
      <div class="card-body">
        <form action="<?php echo site_url('Adm/proses_input_mhs')?>" method="post" enctype="multipart/form-data">
          <div class="form-group">
            <div class="form-row">
              <div class="col-md-6">
                <label for="NIM">NIM</label>
                <input name ="nim" class="form-control" id="nim" type="text" aria-describedby="nameHelp">
              </div>
              <div class="col-md-6">
                <label for="Nama">Nama</label>
                <input name ="nama" class="form-control" id="nama" type="text" aria-describedby="nameHelp">
              </div>
			  <div class="col-md-6">
                <label for="Prodi">Prodi</label>
                <input name ="prodi" class="form-control" id="prodi" type="text" aria-describedby="nameHelp">
              </div>
			  <div class="col-md-6">
                <label for="exampleInputTTL">Tempat Lahir</label>
                <input name ="tempat_lahir" class="form-control" id="tempat_lahir" type="text" aria-describedby="nameHelp">
              </div>
			  <div class="col-md-6">
                <label for="exampleInputTTL">Tanggal Lahir</label>
                <input name ="tgl_lahir" class="form-control" id="tgl_lahir" type="text" aria-describedby="nameHelp">
              </div>
			  <div class="col-md-6">
                <label for="nama_ortu">Nama Orang Tua</label>
                <input name ="nama_ortu" class="form-control" id="nama_ortu" type="text" aria-describedby="nameHelp">
              </div>
			   <div class="col-md-6">
                <label for="pekerjaan_ortu">Pekerjaan Orang Tua</label>
                <input name ="pekerjaan_ortu" class="form-control" id="pekerjaan_ortu" type="text" aria-describedby="nameHelp">
              </div>
			  <div class="col-md-6">
                <label for="alamat">Alamat</label>
                <input name ="alamat" class="form-control" id="alamat" type="text" aria-describedby="nameHelp">
              </div>
			  <div class="col-md-6">
                <label for="password">Password</label>
                <input name ="password" class="form-control" id="password" type="text" aria-describedby="nameHelp">
              </div>
			  <div class="col-md-6">
                <label for="foto_mhs">Foto</label>
                <input name ="file-foto" class="form-control" type="file" aria-describedby="nameHelp">
              </div>
            </div>
          </div> 
          <button type="submit" class="btn btn-primary btn-block">Simpan</button>
        </form>
       
      </div>
    </div>
  </div>
   <?php $this->load->view('./footer');?>
