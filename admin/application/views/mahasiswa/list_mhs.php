<?php $this->load->view('./header');?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="index.html">Dashboard</a>
        </li>
        <li class="breadcrumb-item active">Data Mahasiswa</li>
      </ol>
	  <p><a href="<?php echo site_url('Adm/input_mhs')?>" class="btn btn-primary">Tambah Data Mahasiswa</a></p>
      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i> Data Table Mahasiswa</div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>No</th>
                  <th>NIM</th>
                  <th>Nama</th>
                  <th>Prodi</th>
                  <th>Tempat Lahir</th>
                  <th>Tanggal Lahir</th>
                  <th>Nama Orangtua</th>
                  <th>Pekerjaan Orangtua</th>
                  <th>Alamat</th>
                  <th>Password</th>
                  <th>Foto</th>
                  <th>Action</th>
                </tr>
              </thead>
            <tbody>
				<?php 
				$no=1;
				foreach($mahasiswa as $m){?>
					<tr>
					  <td><?php echo $no++; ?></td>
					  <td><?php echo $m->nim ?></td>
					  <td><?php echo $m->nama ?></td>
					  <td><?php echo $m->prodi ?></td>
					  <td><?php echo $m->tempat_lahir ?></td>
					  <td><?php echo $m->tgl_lahir ?></td>
					  <td><?php echo $m->nama_ortu ?></td>
					  <td><?php echo $m->pekerjaan_ortu ?></td>
					  <td><?php echo $m->alamat ?></td>
					  <td><?php echo $m->password ?></td>
					  <td><img src="<?php echo base_url();?>../assets/foto/<?php echo $m->foto_mhs; ?>" class="img-responsive" width='60' height='60' /></td>
					  <td>
						  <a href="<?php echo site_url('Adm/hapus_mhs/'. $m->nim)?>" class="btn-sm btn-danger" onclick="return confirm('Apakah Anda Yakin Akan Menghapus Data Ini ?') ">Hapus</a><br><br>
						  <a href="<?php echo site_url('Adm/edit_mhs/'. $m->nim)?>" class="btn-sm btn-warning">Edit</a>
						</td>
					</tr>
                <?php }?>
              </tbody>
            </table>
          </div>
        </div>
        <!-- <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>-->
      </div>
    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
<?php $this->load->view('./footer');?>
    