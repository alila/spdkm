<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>SPDKM UNJAYA Admin</title>
  <!-- Bootstrap core CSS-->
  <link href="<?php echo base_url();?>/assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="<?php echo base_url();?>/assets/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Custom styles for this template-->
  <link href="<?php echo base_url();?>/assets/css/sb-admin.css" rel="stylesheet">
  <link href="<?php echo base_url();?>/assets/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
   <link href="<?php echo base_url('assets/date_picker_bootstrap/bootstrap-datetimepicker.min.css') ?>" rel="stylesheet" media="screen">
	<style>
		.bg{
			background:  Darkgoldenrod;
			}
		.bg1{
			background: Ivory;
		}
	</style>
</head>

<body class="fixed-nav sticky-footer bg1" id="page-top">
  <!-- Navigation-->
  <nav class="navbar navbar-expand-lg navbar-dark bg fixed-top" id="mainNav">
  <b><a class="navbar-brand" href="#"><i class="fa fa-fw fa-graduation-cap"></i>S.P.D.K.M UNJAYA Yogyakarta</a></b>
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
      <ul class="navbar-nav navbar-sidenav" id="exampleAccordion">
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Dashboard">
          <a class="nav-link" href="<?php echo site_url('Adm/dashboard');?>">
            <i class="fa fa-fw fa-dashboard"></i>
            <span class="nav-link-text">Dashboard</span>
          </a>
        </li>
		
		<li class="nav-item" data-toggle="tooltip" data-placement="right" title="Tables">
          <a class="nav-link" href="<?php echo site_url('Adm');?>">
            <i class="fa fa-fw fa-user-circle"></i>
            <span class="nav-link-text">Admin Profil</span>
          </a>
        </li>
		
		<li class="nav-item" data-toggle="tooltip" data-placement="right" title="Example Pages">
          <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseExamplePages" data-parent="#exampleAccordion">
            <i class="fa fa-fw fa-list"></i>
            <span class="nav-link-text">Data User</span>
          </a>
          <ul class="sidenav-second-level collapse" id="collapseExamplePages">
            <li>
              <a href="<?php echo site_url('Adm/tampil_mhs');?>">Mahasiswa</a>
            </li> 
          </ul>
        </li>
		
		 <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Components">
          <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseComponents" data-parent="#exampleAccordion">
            <i class="fa fa-fw fa-book"></i>
            <span class="nav-link-text">Laporan</span>
          </a>
          <ul class="sidenav-second-level collapse" id="collapseComponents">
			<li>
              <a href="<?php echo site_url('Adm/laporan_aktif'); ?>">Laporan Aktif Kuliah</a>
            </li>
            <li>
              <a href="<?php echo site_url('Adm/laporan_magang'); ?>">Laporan Magang</a>
            </li>
			<li>
              <a href="<?php echo site_url('Adm/laporan_penelitian'); ?>">Laporan Penelitian</a>
            </li>
			<li>
              <a href="<?php echo site_url('Adm/laporan_kkn'); ?>">Laporan Dokumen Pengabdian Kemasyarakat</a>
            </li>
          </ul>
        </li>
		
		<li class="nav-item" data-toggle="tooltip" data-placement="right" title="Menu Levels">
          <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseMulti" data-parent="#exampleAccordion">
            <i class="fa fa-fw fa-file"></i>
            <span class="nav-link-text">Surat</span>
          </a>
          <ul class="sidenav-second-level collapse" id="collapseMulti">
            <li>
              <a href="<?php echo site_url('c_dokumen/aktif');?>">Surat Mahasiswa Aktif Kuliah</a>
            </li>
            <li>
              <a href="<?php echo site_url('c_dokumen/magang');?>">Surat Pengantar Magang Kerja</a>
            </li>
            <li>
              <a href="<?php echo site_url('c_dokumen/penelitian');?>">Surat Pengantar Penelitian</a>
            </li>
			 <li>
              <a href="<?php echo site_url('c_dokumen/kkn');?>">Surat Pengantar KKN</a>
            </li>
          </ul>
        </li>
		
      </ul>
	  
      <ul class="navbar-nav sidenav-toggler">
        <li class="nav-item">
          <a class="nav-link text-center" id="sidenavToggler">
            <i class="fa fa-fw fa-angle-left"></i>
          </a>
        </li>
      </ul>
      <ul class="navbar-nav ml-auto">
        <li class="nav-item">
          <form class="form-inline my-2 my-lg-0 mr-lg-2">
            <div class="input-group">
              <input class="form-control" type="text" placeholder="Search for...">
              <span class="input-group-append">
                <button class="btn btn-primary" type="button">
                  <i class="fa fa-search"></i>
                </button>
              </span>
            </div>
          </form>
        </li>
        <li class="nav-item">
          <a class="nav-link" data-toggle="modal" data-target="#exampleModal">
            <i class="fa fa-fw fa-sign-out"></i>Logout</a>
        </li>
      </ul>
    </div>
  </nav>