<?php $this->load->view('./header');?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
       <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="<?php echo site_url('Adm/home');?>">Dashboard</a>
        </li>
       </ol> 
	  
      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i> Data Tabel Surat Pengabdian ke Masyarakat</div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>No</th>
                  <th>ID</th>
				  <th>NIM</th>
                  <th>No Surat</th>
				  <th>Tempat KKN</th>
				  <th>Tanggal Mulai</th>
				  <th>Tanggal Selesai</th>
				  <th>Tertuju</th>
				  <th>Action</th>
                </tr>
              </thead>
              <tbody>
				<?php 
				$no=1;
				foreach($c_dokumen as $sk){?>
					<tr>
					  <td><?php echo $no++; ?></td>
					  <td><?php echo $sk->id_kkn ?></td>
					  <td><?php echo $sk->nim ?></td>
					  <td><?php echo $sk->no_surat ?></td>
					  <td><?php echo $sk->tempat_kkn ?></td>
					  <td><?php echo $sk->tgl_mulai ?></td>
					  <td><?php echo $sk->tgl_selesai ?></td>
					  <td><?php echo $sk->tertuju ?></td>
					  <td>
						  <!-- <a href="<?php echo site_url('c_dokumen')?>" class="btn-sm btn-warning">Konfirmasi</a> -->
              <p><a href="<?php echo site_url('c_dokumen/edit_kkn/'.$sk->id_kkn)?>" class="btn-sm btn-warning">Edit</a></p>
              <p><a href="<?php echo site_url('c_dokumen/cetak_kkn/'.$sk->id_kkn)?>" class="btn-sm btn-success">Print</a></p>
						  <p><a href="<?php echo site_url('c_dokumen/hapus_kkn/'.$sk->id_kkn)?>" class="btn-sm btn-danger">Hapus</a></p>
						</td>
					</tr>
                <?php }?>
              </tbody>
            </table>
          </div>
        </div>
        <!-- <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>-->
      </div>
    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
<?php $this->load->view('./footer');?>
    