<?php $this->load->view('./header')?>

<body class="bg-dark">
  <div class="container">
    <div class="card card-register mx-auto mt-5">
      <div class="card-header">Form Pengajuan<br>
	   Surat Pengantar Penelitian</div>
      <div class="card-body">
        <form action="<?php echo site_url('c_dokumen')?>" method="post" enctype="multipart/form-data">
          <div class="form-group">
            <div class="form-row">
			  <div class="col-md-6">
                <label for="exampleInputProdi">Prodi</label>
                <input name ="prodi" class="form-control" id="exampleInputProdi" type="text" aria-describedby="nameHelp">
              </div>
			  <div class="col-md-6">
                <label for="exampleInputalamat">Tujuan</label>
                <input name ="tujuan" class="form-control" id="exampleInputalamat" type="text" aria-describedby="nameHelp">
              </div>
			  
			   <div class="col-md-6">
			 <div class="form-group input-append date form_datetime">
                        <label for="exampleInputEmail1">Tanggal Mulai</label>
                        <input class="form-control"  type="text" value="" name="tgl_mulai" readonly>
                        <span class="add-on"><i class="icon-th"></i></span>
                    </div> </div>
			<div class="col-md-6">		
			<div class="form-group input-append date form_datetime">
                        <label for="exampleInputEmail1">Tanggal Selesai</label>
                        <input class="form-control"  type="text" value="" name="tgl_selesai" readonly>
                        <span class="add-on"><i class="icon-th"></i></span>
                    </div> </div>
					
            </div>
          </div> 
		 
			<button type="submit" class="btn btn-primary btn-block">Pengajuan</button>
			<div class="btn btn-warning btn-block"><a href="<?php echo site_url('Adm/home')?>">Home</a></div>

		</form>   
      </div>
    </div>
  </div>
  
  <?php $this->load->view('./footer')?>
