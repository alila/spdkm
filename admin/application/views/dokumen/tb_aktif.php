<?php $this->load->view('./header');?>
  <div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
       <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="<?php echo site_url('Adm/home');?>">Dashboard</a>
        </li>
       </ol> 
	  
      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i> Data Tabel Surat Keterangan Aktif Kuliah Mahasiswa</div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>No</th>
                  <th>ID</th>
                  <th>NIM</th>
                  <th>No Surat</th>
                  <th>Keperluan</th>
                  <th>Tanggal</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
				<?php 
				$no=1;
				foreach($c_dokumen as $sa){?>
					<tr>
						<td><?php echo $no++; ?></td>
						<td><?php echo $sa->id_aktif ?></td>
						<td><?php echo $sa->nim ?></td>
						<td><?php echo $sa->no_surat ?></td>
						<td><?php echo $sa->keperluan ?></td>
						<td><?php echo $sa->tgl_pengajuan ?></td>
						<td>
							<a class="btn-sm btn-warning" style="float:right" href="<?php echo site_url('Dokumen/edit/'.$sa->id_aktif) ?>">Edit</a>
							<a class="btn-sm btn-success" style="float:right" href="<?php echo site_url('c_dokumen/cetak_aktif/'.$sa->id_aktif) ?>">Print</a>
                            <a class="btn-sm btn-danger" style="float:right" href="<?php echo site_url('c_dokumen/hapus_aktif/'.$sa->id_aktif) ?>">Hapus</a> 
							<!-- <a class="btn-sm btn-info" style="float:right" href="<?php echo site_url('c_dokumen/cetak_aktif/'.$sa->id_aktif) ?>">Tambah</a> -->
						</td>
						
					</tr>
                <?php }?>
              </tbody>
            </table>
          </div>
        </div>
        <!-- <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>-->
      </div>
    </div>
	
	
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
<?php $this->load->view('./footer');?>
    