<?php $this->load->view('./header'); ?>
<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="<?php echo site_url('Adm/home'); ?>">Dashboard</a>
            </li>
        </ol> 

        <div class="card mb-3">
            <div class="card-header">
                <i class="fa fa-table"></i> Data Tabel Surat Magang</div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>ID</th>
								<th>Kode Formulir</th>
                                <th>No Surat</th>
                                <th>Nama Instansi</th>
                                <th>Tertuju</th>
                                <th>Tanggal Mulai</th>
                                <th>Tanggal Selesai</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $no = 1;
                            foreach ($c_dokumen as $sm) {
                                ?>
                                <tr>
                                    <td><?php echo $no++; ?></td>
                                    <td><?php echo $sm->id_magang ?></td>
									<td><?php echo $sm->kode_form ?></td>
                                    <td><?php echo $sm->no_surat ?></td>
                                    <td><?php echo $sm->nama_instansi ?></td>
                                    <td><?php echo $sm->tertuju ?></td>
                                    <td><?php echo $sm->tgl_mulai ?></td>
                                    <td><?php echo $sm->tgl_selesai ?></td>
                                    <td>
                                        <p><a href="<?php echo site_url('c_dokumen/cetak_magang/'.$sm->id_magang) ?>" class="btn-sm btn-success">Print</a></p>
                                        <!-- <p><a href="<?php echo site_url('c_dokumen/konfirmasi_magang/' . $sm->id_magang) ?>" class="btn-sm btn-info">Konfirmasi</a></p> -->
                                        <p><a href="<?php echo site_url('c_dokumen/edit_magang/'.$sm->id_magang) ?>" class="btn-sm btn-warning">Edit</a></p>
										<p><a href="<?php echo site_url('c_dokumen/hapus_magang/'.$sm->id_magang) ?>" class="btn-sm btn-danger">Hapus</a></p>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>-->
        </div>
    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
    <?php $this->load->view('./footer'); ?>
    