<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dokumen extends CI_Controller{
	
	function tampil_surat_aktif(){
		$data['surat_aktif'] = $this->Aktif_m->data_aktif();
		$this->load->view('dokumen/tb_aktif', $data);
	}
	
	function edit($id){
		$data['surat_aktif'] = $this->M_aktif->edit_aktif($id)->result();
		$this->load->view('edit/edit_aktif', $data);
	}
	
	function proses_edit(){
		$id=$this->input->post('id_dok_surat');
		$data = array (
			'no_surat'=>$this->input->post('no_surat'),
			'keperluan'=>$this->input->post('keterangan'),
			'tgl_pengajuan'=>$this->input->post('tgl'),
		);
		$this->M_aktif->proses_edit_aktif($id, $data);
		redirect('c_dokumen/aktif');
	}
	function cetak_aktif($nim){
		$data['surat_aktif']= $this->Aktif_m->aktif($nim);
		$this->load->view('cetak/cetak_aktif', $data);
	}
}