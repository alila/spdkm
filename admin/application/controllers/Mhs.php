<?php
class Mhs extends CI_Controller{
	function __construct(){
		parent::__construct();
			if(!$this->session->userdata('id')){
				$this->load->helper(array('form', 'url'));
				$this->load->library('form_validation');
				redirect('Login');
		}
	}

	function index(){
		$nip=$this->session->userdata('id');
		$data['admin'] = $this->Adm_m->list_adm($nip);
		$this->load->view('admin/list_adm', $data);
	}
	

	function proses_input(){
		$nama_file = $nim.time();
		$config['upload_path']=FCPATH.'../assets/foto/';	
		$config['allowed_types']='jpg|png|doc|pdf'; 
		$config['file_name']=$nama_file;
		$this->upload->initialize($config);
		
		//upload
		if($this->upload->do_upload('file-foto')){
			$foto = $this->upload->data();
			
		$data = array(
		'nim'=> $this->input->post('nip'),
		'nama'=> $this->input->post('nama'),
		'password'=> $this->input->post('password'),
		'foto_adm'=> $foto['file_name']
		);
		$this->Adm_m->proses_input_db($data);
		redirect('Mhs');
		
		} else {
			$error = array('error'=>$this->upload->display_errors());
			echo json_encode($error);
		}
	}
}

?>