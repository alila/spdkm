<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class c_dokumen extends CI_Controller {

    function __construct() {
        parent::__construct();
        if (!$this->session->userdata('id')) {
            redirect('Login');
        }
    }

    //SURAT AKTIF KULIAH
    function aktif() {
        $data['c_dokumen'] = $this->M_aktif->aktif();
        $this->load->view('dokumen/tb_aktif', $data);
    }

    function cetak_aktif($id) {
        $data['surat_aktif'] = $this->M_aktif->aktif($id);
        $this->load->view('cetak/cetak_aktif', $data);
    }

    function hapus_aktif($id){
		$this->M_aktif->hapus_aktif($id);
		redirect('c_dokumen/aktif', $data);
	}

    //SURAT MAGANG
    function magang() {
        $data['c_dokumen'] = $this->M_magang->magang();
        $this->load->view('dokumen/tb_magang', $data);
    }

    function konfirmasi_magang($id) {
        $data=array(
            'status'=> 1
        );
        $this->M->magang->update_magang($data, $id);
        redirect('c_dokumen/magang');
    }

    function cetak_magang($id) {
        $data['c_dokumen'] = $this->M_magang->cetak_magang($id);
        $data['c_dokumen1'] = $this->M_magang->cetak_mhs_magang($id);
        $this->load->view('cetak/cetak_magang', $data);
    }

	function edit_magang($id){
		$data['c_dokumen'] = $this->M_magang->edit_magang($id);
		$this->load->view('edit/edit_magang', $data);
	}

	function proses_edit_magang($id){
        $data = array(
            'nama_instansi' => $this->input->post('nama_instansi'),
            'tgl_mulai'     => $this->input->post('tgl_mulai'),
            'tgl_selesai'   => $this->input->post('tgl_selesai'),
            'alamat_instansi' => $this->input->post('alamat'),
            'tertuju'     => $this->input->post('tertuju'),
            'no_telp'   => $this->input->post('no_hp')
            );	
			
		$this->M_magang->proses_edit_db($data, $id);
		redirect('c_dokumen/magang');
    }
    
    function hapus_magang($id){
		$this->M_magang->hapus_magang($id);
		redirect('c_dokumen/magang', $data);
	}

    //SURAT PENELITIAN
    function penelitian() {
        $data['c_dokumen'] = $this->M_penelitian->penelitian();
        $this->load->view('dokumen/tb_penelitian', $data);
    }

    function cetak_penelitian($id) {
        $data['c_dokumen'] = $this->M_penelitian->cetak_penelitian($id);
        $data['c_dokumen1'] = $this->M_penelitian->cetak_mhs_penelitian($id);
        $this->load->view('cetak/cetak_penelitian', $data);
    }
    
    function hapus_penelitian($id){
        $this->M_penelitian->hapus_penelitian($id);
        redirect('c_dokumen/penelitian', $data);
    }

    function edit_penelitian($id){
        $data['c_dokumen'] = $this->M_penelitian->edit_penelitian($id);
        print_r($data);
        $this->load->view('edit/edit_penelitian', $data);
    }

    function proses_edit_penelitian($id){
        // print_r($this->input->post());die();
        $data = array(
            'nim' => $this->input->post('nim'),
            'tempat_penelitian' => $this->input->post('tempat_penelitian'),
            'tgl_mulai'   => $this->input->post('tgl_mulai'),
            'tgl_selesai' => $this->input->post('tgl_selesai'),
            'tertuju'     => $this->input->post('tertuju'),
            // 'kode_surat'   => $this->input->post('kode_surat')
            );  
            
        $this->M_penelitian->proses_edit_db($data, $id);
        redirect('c_dokumen/penelitian');
    }

    //SURAT KKN
    function kkn() {
        $nim = $this->session->userdata('id');
        $data['c_dokumen'] = $this->M_kkn->kkn();
        $this->load->view('dokumen/tb_kkn', $data);
    }

    function cetak_kkn($id) {
        $data['c_dokumen'] = $this->M_kkn->cetak_kkn($id);
        $data['c_dokumen3'] = $this->M_kkn->cetak_mhs_kkn($id);
        $this->load->view('cetak/cetak_kkn', $data);
    }

    function edit_kkn($id){
        $data['c_dokumen'] = $this->M_kkn->edit_kkn($id);
        $this->load->view('edit/edit_kkn', $data);
    }

    function proses_edit_kkn($id){
        $data = array(
            // 'no_surat' => $this->input->post('no_surat'),
            'tempat_kkn' => $this->input->post('tempat_kkn'),
            'tgl_mulai'   => $this->input->post('tgl_mulai'),
            'tgl_selesai' => $this->input->post('tgl_selesai'),
            'tertuju'     => $this->input->post('tertuju'),
            // 'kode_surat'   => $this->input->post('kode_surat')
            );  
            
        $this->M_kkn->proses_edit_db($data, $id);
        redirect('c_dokumen/kkn');
    }
    
    function hapus_kkn($id){
        $this->M_kkn->hapus_kkn($id);
        redirect('c_dokumen/kkn', $data);
    }

}

?>