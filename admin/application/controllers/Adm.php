<?php
class Adm extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
			if(!$this->session->userdata('id')){
				redirect('Login');
		}
	}
	function dashboard(){
		$this->load->view('blank');
	}
	// LAPORAN
	function laporan_magang(){
		$this->load->view('laporan/form_magang');
	}
	function tampil_laporan_magang(){
		$start =  $this->input->post('tgl_mulai');
		$end =  $this->input->post('tgl_selesai');
		$data['magang']=$this->m_laporan_magang->laporan_magang($start,$end);
		$this->load->view('laporan/laporan_magang', $data);
	}
	function laporan_aktif(){
		$this->load->view('laporan/form_aktif');
	}
	function tampil_laporan_aktif(){
		$start =  $this->input->post('tgl_mulai');
		$end =  $this->input->post('tgl_selesai');
		$data['aktif']=$this->m_laporan_aktif->laporan_aktif($start,$end);
		$this->load->view('laporan/laporan_aktif', $data);
	}
	function laporan_penelitian(){
		$this->load->view('laporan/form_penelitian');
	}
	function tampil_laporan_penelitian(){
		$start =  $this->input->post('tgl_mulai');
		$end =  $this->input->post('tgl_selesai');
		$data['penelitian']=$this->m_laporan_penelitian->laporan_penelitian($start,$end);
		$this->load->view('laporan/laporan_penelitian', $data);
	}
	function laporan_kkn(){
		$this->load->view('laporan/form_kkn');
	}
	function tampil_laporan_kkn(){
		$start =  $this->input->post('tgl_mulai');
		$end =  $this->input->post('tgl_selesai');
		$data['kkn']=$this->m_laporan_kkn->laporan_kkn($start,$end);
		$this->load->view('laporan/laporan_kkn', $data);
	}
	
	// ADMIN
	function index(){
		$nip=$this->session->userdata('id');
		$data['admin'] = $this->Adm_m->list_adm($nip);
		$this->load->view('admin/list_adm', $data);
	}
	function admin(){
		$this->load->view('admin/admin');
	}
	function view_input_adm(){
		$data['dokumen']=$this->Adm_m->input_adm();
		$this->load->view('admin/admin', $data);	
	}
	function proses_input(){
		
		$nama_file = 'file-foto'.time();
		$config['upload_path']=FCPATH.'../assets/foto/';	
		$config['allowed_types']='jpg|png|doc|pdf'; 
		$config['file_name']=$nama_file;
		$this->upload->initialize($config);
		
		//upload
		if($this->upload->do_upload('file-foto')){
			$foto = $this->upload->data();
			
		$data = array(
		'nip'=> $this->input->post('nip'),
		'nama'=> $this->input->post('nama'),
		'password'=> $this->input->post('password'),
		'foto_adm'=> $foto['file_name']
		);
		$this->Adm_m->proses_input_db($data);
		redirect('Adm');
		
		} else {
			$error = array('error'=>$this->upload->display_errors());
			echo json_encode($error);
		}
	}
	function hapus_adm($nip){
		$this->Adm_m->hapus_adm($nip);
		redirect('Adm');
	}
	function edit_adm($nip){
		$data['admin'] = $this->Adm_m->edit_adm($nip);
		$this->load->view('admin/edit', $data);
	}
	function proses_edit(){
		$config = [
			'file_name' => 'file-foto'.time(),
			'upload_path' => './assets/foto/',
			'allowed_types' => 'gif|jpeg|jpg|png',
			'max_size' => 2000
		  ];
  
		$this->load->library('upload', $config);

		$this->upload->initialize($config);

		if($this->upload->do_upload('file_foto')){			
			$file = $this->upload->data();
			$nip = $this->input->post('nip');
			$data = array(
				'nama'=> $this->input->post('nama'),
				'password'=> $this->input->post('password'),
				'foto_adm'=>$file['file_name']

				);	
			
		$this->Adm_m->proses_edit_db($data, $nip);
		redirect('Adm');
		} else {
			$error = array('error'=>$this->upload->display_errors());
			echo json_encode($error);
		}
	}
	// ADMIN END

	// MAHASISWA Start
	function input_Mhs(){
		$this->load->view('mahasiswa/mahasiswa');
	}

	function tampil_mhs(){
		$data['mahasiswa'] = $this->Adm_m->list_mhs();
		$this->load->view('mahasiswa/list_mhs', $data);
	}

	function proses_input_mhs(){
		//konfigurasi
		$nama_file = 'file-foto'.time();
		$config['upload_path']=FCPATH.'../assets/foto/';	
		$config['allowed_types']='jpg|png|doc|pdf'; 
		$config['file_name']=$nama_file;
		$this->upload->initialize($config);
		//upload
		if($this->upload->do_upload('file-foto')){
			$foto = $this->upload->data();
			
		$data = array(
			'nim'=> $this->input->post('nim'),
			'nama'=> $this->input->post('nama'),
			'prodi'=> $this->input->post('prodi'),
			'tempat_lahir'=> $this->input->post('tempat_lahir'),
			'tgl_lahir'=> $this->input->post('tgl_lahir'),
			'nama_ortu'=> $this->input->post('nama_ortu'),
			'pekerjaan_ortu'=> $this->input->post('pekerjaan_ortu'),
			'alamat'=> $this->input->post('alamat'),
			'password'=> $this->input->post('password'),
			'foto_mhs'=>$foto['file_name']
			);
		$this->Adm_m->proses_input_mhs_db($data);
		redirect('Adm/tampil_mhs');	
		} else {
			$error = array('error'=>$this->upload->display_errors());
			echo json_encode($error);
		}	
	}
	function hapus_mhs($nim){
		$this->Adm_m->hapus_mhs($nim);
		redirect('Adm/tampil_mhs');
	}
	function edit_mhs($nim){
		$data['mahasiswa'] = $this->Adm_m->edit_mhs($nim);
		$this->load->view('mahasiswa/edit', $data);
	}
	function proses_edit_mhs(){
		$nama_file = 'file-foto'.time();
		$config['upload_path']=FCPATH.'../assets/foto/';	
		$config['allowed_types']='jpg|png|doc|pdf'; 
		$config['file_name']=$nama_file;
		$this->upload->initialize($config);
		if($this->upload->do_upload('file-foto')){
			$foto = $this->upload->data();
			
		$nim = $this->input->post('nim');
		$data = array(
			'nama'=> $this->input->post('nama'),
			'prodi'=> $this->input->post('prodi'),
			'tempat_lahir'=> $this->input->post('tempat_lahir'),
			'tgl_lahir'=> $this->input->post('tgl_lahir'),
			'nama_ortu'=> $this->input->post('nama_ortu'),
			'pekerjaan_ortu'=> $this->input->post('pekerjaan_ortu'),
			'alamat'=> $this->input->post('alamat'),
			'password'=> $this->input->post('password'),
			'foto_mhs'=>$foto['file_name']
			);
		$this->Adm_m->proses_edit_mhs_db($data, $nim);
		redirect('Adm/tampil_mhs');
		}else{
			$error=array(
			'error'=>$this->upload->display_errors());
			echo json_encode($error);
			
		}
	}
	// MAHASISWA End
	
}

?>