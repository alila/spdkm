<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class logout extends CI_Controller {
	function index()
	{
		//$this->session->sess_destroy();
		$this->session->unset_userdata('id');
		$this->session->unset_userdata('nama');
		redirect('login');
	}
}
?>